<?php

namespace App\Jobs;

use App\Models\Bible;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GetDataFromSabda implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $id, $ver, $text;

    public function __construct($id, $ver, $text)
    {
        $this->id = $id;
        $this->ver = $ver;
        $this->text = $text;
    }

    public function handle()
    {
        $delete = Bible::where('version_id', $this->id)->delete();
        $book = 1;
        $chapter = 1;
        // $book = 43;
        // $chapter = 9;

        while ($book < 67) {
            $url = "https://alkitab.sabda.org/api/chapter.php?book=" . $book . "&chapter=" . $chapter . "&ver=" . $this->ver;
            echo $url . '<br/>';
            $xml = file_get_contents($url);
            $xml = preg_replace('#&(?=[a-z_0-9]+=)#', '&amp;', $xml);
            $text = simplexml_load_string($xml);

            foreach ($text->verses->verse as $v) {
                $bible = new Bible;
                $bible->book_num = $text->book;
                $bible->book = $text->bookname;
                $bible->chapter = $text->chapter;
                $bible->verse_number = $v->number;
                $bible->verse_text = $v->text;
                $bible->version_id = $this->id;
                $bible->save();
            }

            if ($chapter == $text->chapter_count) {
                $book++;
                $chapter = 1;
            } else $chapter++;
        }

        return true;
    }
}
