<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Church extends Model
{
    use HasFactory, SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $guarded = [];

    public function sinode()
    {
        return $this->belongsTo(Sinode::class, 'sinode_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
