<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes;
    protected $dates = ['deleted_at', 'email_verified_at'];
    protected $guarded = [];
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function role()
    {
        return $this->belongsTo(UserRole::class, 'role_id');
    }

    public function lastActivity()
    {
        return $this->hasOne(UserLog::class, 'user_id')->latestOfMany();
    }

    public function pointHistory()
    {
        return $this->hasMany(PointHistory::class, 'user_id');
    }
}
