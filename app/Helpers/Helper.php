<?php

namespace App\Helpers;

use App\Models\Bible;
use App\Models\Kepengurusan;
use App\Models\Mapping;
use App\Models\UserLog;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class Helper
{
    public static function recordUserLog($id, $status, $lat = null, $lng = null)
    {
        UserLog::create([
            'user_id' => $id,
            'status' => $status,
            'lat' => $lat,
            'lng' => $lng
        ]);
    }

    public static function uploadFile($file, $fileName, $folder)
    {
        $size = $file->getSize();
        $ext = '.' . $file->getClientOriginalExtension();

        $fixedFileName = preg_replace("/[^a-zA-Z0-9.]/", "", $fileName);

        $destinationPath = public_path() . $folder;
        $uploadedname = time() . $fixedFileName . $ext;

        $file->move($destinationPath, $uploadedname);
        $destinationPath = env('APP_URL') . $folder;

        return $destinationPath . $uploadedname;
    }

    public static function generateQr($fileName, $folder)
    {
        $qrCode = QrCode::format("svg")
            ->size(500)
            ->merge('/public/logo.png')
            ->generate(env("APP_URL") . '/' . $folder . '/' . $fileName, public_path() . '/qr/' . $fileName . ".svg");
    }

    public static function cleanStr($string)
    {
        // Replaces all spaces with hyphens.
        $string = str_replace(' ', '-', $string);

        // Removes special chars.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        // Replaces multiple hyphens with single one.
        $string = preg_replace('/-+/', '-', $string);

        return $string;
    }
}
