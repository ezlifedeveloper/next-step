<?php

namespace App\Http\Controllers\api;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Church;
use App\Models\ChurchRequest;
use App\Models\Sinode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * @group Church Management
 *
 * APIs for managing church
 * @authenticated
 */
class ChurchController extends Controller
{
    /**
     * Get Church Sinode
     *
     * Get Church Sinode Data, on success you'll get a 200 OK response.
     *
     * @queryParam search string Church name to be searched. Example: GP
     * @queryParam limit int Number of data shown. Example: 3
     * @queryParam page int Selected page to be shown. Example: 1
     *
     * @responseField success The status of this API request.
     * @responseField result Description of this API request.
     * @responseField data Church Sinode Data sorted by distance.
     */
    public function sinode(Request $request)
    {
        $search = $request->search ?? null;
        $limit = $request->limit ?? null;
        $page = $request->page ?? null;

        $data = Sinode::select('name', 'desc')
            ->when($search, function ($query) use ($search) {
                return $query->where('name', 'like', '%' . $search . '%')
                    ->orWhere('desc', 'like', '%' . $search . '%');
            })->when($limit, function ($query) use ($limit) {
                return $query->limit($limit);
            })->when($page, function ($query) use ($page) {
                return $query->offset($page);
            })->orderBy('name', 'asc')->get();

        return response()->json(['success' => true, 'result' => 'Successfully Get Church Sinode Data', 'data' => $data], 200);
    }

    /**
     * Get Church
     *
     * Get Church Data sorted by location given, on success you'll get a 200 OK response.
     * Return 422 when parameter is not filled.
     *
     * @bodyParam search string required Church name to be searched. Example: GKI
     * @bodyParam lat string required Latitude of user location. Example: -6.1817619
     * @bodyParam lng string required Longitude of user location. Example: 106.956194
     * @bodyParam limit int Number of data shown. Example: 5
     * @bodyParam page int Selected page to be shown. Example: 2
     *
     * @responseField success The status of this API request.
     * @responseField result Description of this API request.
     * @responseField data Church Data sorted by distance.
     */
    public function get(Request $request)
    {
        if (!$request->filled('search') || !$request->filled('lat') || !$request->filled('lng')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter'], 422);
        }

        $search = $request->search;
        $lat = $request->lat;
        $lng = $request->lng;
        $limit = $request->limit ?? null;
        $page = $request->page ?? null;
        $earths_radius = 6371;
        $distance = 100;

        $data = Church::selectRaw('*, (ACOS(COS(RADIANS(' . $lat . '))* COS(RADIANS(lat))* COS(RADIANS(lng)-RADIANS(' . $lng . '))+SIN(RADIANS(' . $lat . '))*SIN(RADIANS(lat)))*6371) AS distance')
            ->when($search, function ($query) use ($search) {
                return $query->where('name', 'like', '%' . $search . '%')
                    ->orWhere('address', 'like', '%' . $search . '%')
                    ->orWhere('services', 'like', '%' . $search . '%');
            })->when($limit, function ($query) use ($limit) {
                return $query->limit($limit);
            })->when($page, function ($query) use ($page) {
                return $query->offset($page);
            })->orderBy('distance', 'asc')->get();

        return response()->json(['success' => true, 'result' => 'Successfully Get Church Data', 'data' => $data], 200);
    }

    /**
     * Send Church Request
     *
     * Get Church Data sorted by location given, on success you'll get a 200 OK response.
     * Return 422 when parameter is not filled.
     *
     * @bodyParam user int required User ID. Example: 1
     * @bodyParam sinode int Sinode ID. Example: 1
     * @bodyParam church int Church ID when User update a church, otherwise leave it blank. Example: 1
     * @bodyParam name string Church's name. Example: GPIB Persatuan
     * @bodyParam address string Church's address. Example: Jl. IIni iit ini ituu, Kota Begn, Beigt
     * @bodyParam lat string Latitude of church. Example: -6.1817619
     * @bodyParam lng string Longitude of church. Example: 106.956194
     * @bodyParam phone string Church phone. Example: 5
     * @bodyParam service int Church service time. Example: 2
     * @bodyParam website int Church website. Example: https://ezlife.id
     * @bodyParam android int Church android app link. Example: https://ezlife.id
     * @bodyParam ios int Church ios app link. Example: https://ezlife.id
     * @bodyParam file file Church image.
     *
     * @responseField success The status of this API request.
     * @responseField result Description of this API request.
     * @responseField data Church Data sorted by distance.
     */
    public function request(Request $request)
    {
        if (!$request->filled('user')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter'], 422);
        }

        $data = ChurchRequest::create([
            'user_id' => $request->user,
            'sinode_id' => $request->sinode,
            'church_id' => $request->church,
            'name' => $request->name,
            'address' => $request->address,
            'phone' => $request->phone,
            'services' => $request->service,
            'lat' => $request->lat,
            'lng' => $request->lng,
            'android' => $request->android,
            'website' => $request->website,
            'ios' => $request->ios,
            'status' => 0,
        ]);

        if ($data && $request->has('file')) {
            $filePath = Helper::uploadFile($request->file('file'), bcrypt($request->name), '/church/');
            $imgupdate = ChurchRequest::where('id', $data->id)->update(['image' => $filePath]);
        }

        return response()->json(['success' => true, 'result' => 'Successfully Submit Church Request', 'data' => $data], 200);
    }
}
