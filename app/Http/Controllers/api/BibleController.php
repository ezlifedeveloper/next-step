<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Bible;
use App\Models\BibleList;
use Illuminate\Http\Request;

/**
 * @group Bible Management
 *
 * APIs for managing bible
 * @authenticated
 */
class BibleController extends Controller
{
    /**
     * Get Bible
     *
     * Get Bible Data based on Version, on success you'll get a 200 OK response.
     * Return 422 when Version parameter is not filled.
     *
     * @bodyParam version string required The Version Name. Example: TB
     * @bodyParam book string required The Version Name. Example: 2
     *
     * @responseField success The status of this API request.
     * @responseField result Description of this API request.
     * @responseField data Bible Data per passage.
     */
    public function get(Request $request)
    {
        if (!$request->filled('version') || !$request->filled('book')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter'], 422);
        }

        $search = $request->version;
        $search = BibleList::where('name', 'like', $search)->first();
        $data = Bible::where('version_id', $search->id)->where('book_num', $request->book)->get();

        return response()->json(['success' => true, 'result' => 'Successfully Get Bible Data', 'data' => $data], 200);
    }

    /**
     * Get Bible Version Available
     *
     * Get Available Version List, on success you'll get a 200 OK response.
     *
     * @responseField success The status of this API request.
     * @responseField result Description of this API request.
     * @responseField data Version List data.
     */
    public function version()
    {
        $data = BibleList::orderBy('name')->get();

        return response()->json(['success' => true, 'result' => 'Successfully Get Bible Version List', 'data' => $data], 200);
    }
}
