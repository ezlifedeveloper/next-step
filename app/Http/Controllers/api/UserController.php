<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

/**
 * @group User Management
 *
 * APIs for managing User Data
 * @authenticated
 */
class UserController extends Controller
{
    /**
     * Get User List Info
     *
     * Get User List Info, on success you'll get a 200 OK response.
     * Return 422 when parameter is not filled.
     *
     * @bodyParam search string required User name to be searched. Example: Pr
     * @bodyParam limit int Number of data shown. Example: 5
     * @bodyParam page int Selected page to be shown. Example: 2
     *
     * @responseField success The status of this API request.
     * @responseField result Description of this API request.
     * @responseField data User list.
     */
    public function list(Request $request)
    {
        if (!$request->filled('search')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter'], 422);
        }

        $search = $request->search;
        $limit = $request->limit ?? null;
        $page = $request->page ?? null;

        $data = User::when($search, function ($query) use ($search) {
            return $query->where('name', 'like', '%' . $search . '%');
        })->when($limit, function ($query) use ($limit) {
            return $query->limit($limit);
        })->when($page, function ($query) use ($page) {
            return $query->offset($page);
        })->orderBy('name', 'asc')->get();

        return response()->json(['success' => true, 'result' => 'Successfully User list', 'data' => $data], 200);
    }
}
