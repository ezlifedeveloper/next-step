<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\AppVersion;
use App\Models\Church;
use Illuminate\Http\Request;

/**
 * @group App Version Management
 *
 * APIs for checking application and data version
 * @authenticated
 */
class VersionController extends Controller
{
    /**
     * Get Latest App Version Info
     *
     * Get Latest App Version Info, on success you'll get a 200 OK response.
     * Return 422 when parameter is not filled.
     *
     * @bodyParam platform int required Platform to be checked (0: Website, 1: Mobile). Example: 1
     *
     * @responseField success The status of this API request.
     * @responseField result Description of this API request.
     * @responseField data Latest Version data.
     */
    public function app(Request $request)
    {
        if (!$request->filled('platform')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter'], 422);
        }

        $data = AppVersion::where('platform', $request->platform)->latest()->first();

        return response()->json(['success' => true, 'result' => 'Successfully Latest Version Data', 'data' => $data], 200);
    }

    /**
     * Get Latest Church Timestamp
     *
     * Get Latest Church Timestamp, on success you'll get a 200 OK response.
     *
     * @bodyParam date date required Datetime last time the app sync with the cloud data (yyyy-mm-dd hh:mm:ss). Example: 2021-10-13 11:00:00
     *
     * @responseField success The status of this API request.
     * @responseField result Description of this API request.
     * @responseField data All Church that updated more than given date.
     */
    public function church(Request $request)
    {
        $data = Church::whereDate('updated_at', '>', $request->date)->get();

        return response()->json(['success' => true, 'result' => 'Successfully Latest Church Data', 'data' => $data], 200);
    }
}
