<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Models\User;
use App\Models\UserLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $username = $request->username;
        $pwd = $request->password;

        if (Auth::attempt(['username' => $username, 'password' => $pwd])) {
            if(Auth::user()->role_id == 1) return redirect()->route('dashboard');
            else return redirect()->route('churchdashboard');
        } else {
            return redirect()->back()->with('alert-danger', 'Terjadi kesalahan dalam login. Cek kembali Username dan Password Anda');
        }
    }

    public function logout(Request $request)
    {
        Helper::recordUserLog(Auth::id(), 'Web Logout');

        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->back();
    }
}
