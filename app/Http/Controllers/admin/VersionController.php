<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\AppVersion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VersionController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->search;
        $data = AppVersion::with('creator')->when($search, function ($query) use ($search) {
            return $query->where('version', 'like', '%' . $search . '%')
                ->orWhere('created_by', 'like', '%' . $search . '%');
        })->orderBy('date', 'desc')
            ->paginate(20);

        return view('admin.version.index')
            ->with('data', $data)
            ->with('title', 'App Version');
    }

    public function create()
    {
        return view('admin.version.create')
            ->with('title', 'App Version');
    }

    public function store(Request $request)
    {
        $data = AppVersion::create([
            'version' => $request->version,
            'desc' => $request->desc,
            'platform' => $request->platform,
            'date' => $request->date,
            'created_by' => Auth::id()
        ]);

        return redirect()->route('version.index')->withSuccess('Version Created Successfully!');
    }

    public function edit($id)
    {
        $data = AppVersion::find($id);

        return view('admin.version.edit')
            ->with('data', $data)
            ->with('title', 'App Version');
    }

    public function update(Request $request, $id)
    {
        $data = AppVersion::where('id', $id)
            ->update([
                'version' => $request->version,
                'desc' => $request->desc,
                'platform' => $request->platform,
                'date' => $request->date,
                'created_by' => Auth::id()
            ]);

        return redirect()->route('version.index')->withSuccess('Version Updated Successfully!');
    }

    public function restore(Request $request)
    {
        $data = AppVersion::withTrashed()->where('id', $request->id)->restore();

        return redirect()->route('version.index')->withSuccess('Version Restored Successfully!');
    }

    public function destroy($id)
    {
        $data = AppVersion::where('id', $id)->delete();

        return redirect()->route('version.index')->withSuccess('Version Deleted Successfully!');
    }
}
