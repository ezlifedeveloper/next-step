<?php

namespace App\Http\Controllers\admin;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Church;
use App\Models\ChurchRequest;
use App\Models\PointHistory;
use App\Models\Sinode;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use stdClass;

class ChurchRequestController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->search;
        $data = ChurchRequest::orderBy('status')
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        return view('admin.church.request')
            ->with('data', $data)
            ->with('title', 'Church Request');
    }

    public function update(Request $request, $id)
    {
        $data = ChurchRequest::where('id', $id)
            ->update([
                'status' => $request->status
            ]);

        if ($request->status == 2)
            return redirect()->route('churchrequest.index')->withSuccess('Church Rejected!');

        if ($request->status == 1) {
            $point = 0;
            $data = ChurchRequest::where('id', $id)->first();

            if ($data->name) $point += 2;
            if ($data->address) $point += 2;
            if ($data->phone) $point += 4;
            if ($data->lat) $point += 3;
            if ($data->service) $point += 4;
            if ($data->image) $point += 5;

            if ($data->church_id) {
                Church::where('id', $data->church_id)
                    ->update([
                        'sinode_id' => $data->sinode_id,
                        'name' => $data->name,
                        'address' => $data->address,
                        'phone' => $data->phone,
                        'services' => $data->service,
                        'lat' => $data->lat,
                        'lng' => $data->lng,
                        'android' => $data->android,
                        'website' => $data->website,
                        'ios' => $data->ios,
                        'image' => $data->image,
                        'status' => 1
                    ]);

                $update = PointHistory::create(['user_id' => $data->user_id, 'point' => $point, 'desc' => 'Update Church', 'church_id' => $data->church_id]);
                $user = User::where('id', $data->user_id)->increment('point', $point);
                return redirect()->route('churchrequest.index')->withSuccess('Church Data Updated!');
            } else {
                $data = Church::create([
                    'user_id' => $data->user_id,
                    'sinode_id' => $data->sinode_id,
                    'name' => $data->name,
                    'address' => $data->address,
                    'phone' => $data->phone,
                    'services' => $data->service,
                    'lat' => $data->lat,
                    'lng' => $data->lng,
                    'android' => $data->android,
                    'website' => $data->website,
                    'ios' => $data->ios,
                    'image' => $data->image,
                    'status' => 1,
                ]);

                $update = PointHistory::create(['user_id' => $data->user_id, 'point' => $point, 'desc' => 'Suggest Church', 'church_id' => $data->id]);
                $user = User::where('id', $data->user_id)->increment('point', $point);
                return redirect()->route('churchrequest.index')->withSuccess('Church Inserted to Production!');
            }
        }
    }

    public function destroy($id)
    {
        $data = ChurchRequest::where('id', $id)->delete();

        return redirect()->route('churchrequest.index')->withSuccess('Church Deleted Successfully!');
    }
}
