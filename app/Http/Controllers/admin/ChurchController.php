<?php

namespace App\Http\Controllers\admin;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Church;
use App\Models\Sinode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use stdClass;

class ChurchController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->search;
        $data = Church::when(Auth::user()->role_id == 1, function ($query) {
            return $query->withTrashed();
        })->with('sinode')
            ->when($search, function ($query) use ($search) {
                return $query->where('name', 'like', '%' . $search . '%')
                    ->orWhere('address', 'like', '%' . $search . '%');
            })
            ->orderBy('name')
            ->paginate(10);

        return view('admin.church.index')
            ->with('data', $data)
            ->with('title', 'Church Data');
    }

    public function create()
    {
        $sinode = Sinode::orderBy('name')->get();
        return view('admin.church.create')
            ->with('sinode', $sinode)
            ->with('title', 'Church Data');
    }

    public function store(Request $request)
    {
        $data = Church::create([
            'user_id' => Auth::id(),
            'sinode_id' => $request->sinode,
            'name' => $request->name,
            'address' => $request->address,
            'phone' => $request->phone,
            'services' => $request->service,
            'lat' => $request->latitude,
            'lng' => $request->longitude,
            'android' => $request->android,
            'website' => $request->website,
            'ios' => $request->ios,
            'status' => 1,
        ]);

        if ($data && $request->has('file')) {
            $filePath = Helper::uploadFile($request->file('file'), bcrypt($request->name), '/church/');
            $imgupdate = Church::where('id', $data->id)->update(['image' => $filePath]);
        }

        return redirect()->route('churchdata.index')->withSuccess('Church Created Successfully!');
    }

    public function edit($id)
    {
        $data = Church::find($id);
        $sinode = Sinode::orderBy('name')->get();

        return view('admin.church.edit')
            ->with('data', $data)
            ->with('sinode', $sinode)
            ->with('title', 'Church Data');
    }

    public function update(Request $request, $id)
    {
        $data = Church::where('id', $id)
            ->update([
                'sinode_id' => $request->sinode,
                'name' => $request->name,
                'address' => $request->address,
                'phone' => $request->phone,
                'services' => $request->service,
                'lat' => $request->latitude,
                'lng' => $request->longitude,
                'android' => $request->android,
                'website' => $request->website,
                'ios' => $request->ios,
                'status' => 1
            ]);

        if ($data && $request->has('file')) {
            $filePath = Helper::uploadFile($request->file('file'), bcrypt($request->name), '/church/');
            $imgupdate = Church::where('id', $id)->update(['image' => $filePath]);
        }

        return redirect()->route('churchdata.index')->withSuccess('Church Updated Successfully!');
    }

    public function restore(Request $request)
    {
        $data = Church::withTrashed()->where('id', $request->id)->restore();

        return redirect()->route('churchdata.index')->withSuccess('Church Restored Successfully!');
    }

    public function delete(Request $request)
    {
        $data = Church::where('id', $request->id)->forceDelete();

        return redirect()->route('churchdata.index')->withSuccess('Church Deleted Permanently!');
    }

    public function destroy($id)
    {
        $data = Church::where('id', $id)->delete();

        return redirect()->route('churchdata.index')->withSuccess('Church Deleted Successfully!');
    }

    public function dashboard()
    {
        $data['church_data'] = Church::orderBy('id', 'desc')->limit(6)->get();
        $data['church_count'] = Church::all()->count();
        $data['visit_count'] = 0; //Note::all()->count();
        $data['sinode_count'] = Sinode::count(); //Note::all()->count();

        $churchData = Church::all();

        $church = [];

        $i = 0;
        foreach ($churchData as $c) {
            $churchzz = new stdClass;
            $churchzz->lat = (float)$c->lat;
            $churchzz->lng = (float)$c->lng;

            $church[$i]['position'] = $churchzz;
            $church[$i++]['label'] = $c->name;
        }

        return view("admin.church.dashboard")
            ->with('data', $data)
            ->with('church', $church)
            ->with('title', 'Church Dashboard');
    }
}
