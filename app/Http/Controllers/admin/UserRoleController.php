<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserRoleController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->search;
        $data = UserRole::withTrashed()->with('user')
            ->when($search, function ($query) use ($search) {
                return $query->where('name', 'like', '%' . $search . '%')
                    ->orWhere('created_by', 'like', '%' . $search . '%');
            })
            ->orderBy('name')
            ->paginate(10);

        return view('admin.role.index')
            ->with('data', $data)
            ->with('title', 'User Role');
    }

    public function create()
    {
        return view('admin.role.create')
            ->with('title', 'User Role');
    }

    public function store(Request $request)
    {
        $data = UserRole::create([
            'name' => $request->name,
            'created_by' => Auth::user()->name
        ]);

        return redirect()->route('userrole.index')->withSuccess('Role Created Successfully!');
    }

    public function edit($id)
    {
        $data = UserRole::find($id);

        return view('admin.role.edit')
            ->with('data', $data)
            ->with('title', 'User Role');
    }

    public function update(Request $request, $id)
    {
        $data = UserRole::where('id', $id)
            ->update(['name' => $request->name]);

        return redirect()->route('userrole.index')->withSuccess('Role Updated Successfully!');
    }

    public function restore(Request $request)
    {
        $data = UserRole::withTrashed()->where('id', $request->id)->restore();

        return redirect()->route('userrole.index')->withSuccess('Role Restored Successfully!');
    }

    public function delete(Request $request)
    {
        $data = UserRole::where('id', $request->id)->forceDelete();

        return redirect()->route('userrole.index')->withSuccess('Role Deleted Permanently!');
    }

    public function destroy($id)
    {
        $data = UserRole::where('id', $id)->delete();

        return redirect()->route('userrole.index')->withSuccess('Role Deleted Successfully!');
    }
}
