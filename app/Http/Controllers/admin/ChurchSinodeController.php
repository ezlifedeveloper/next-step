<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Sinode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChurchSinodeController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->search;
        $data = Sinode::when(Auth::user()->role_id == 1, function ($query) {
            return $query->withTrashed();
        })->with('church')
            ->when($search, function ($query) use ($search) {
                return $query->where('name', 'like', '%' . $search . '%')
                    ->orWhere('created_by', 'like', '%' . $search . '%');
            })
            ->orderBy('name')
            ->paginate(20);

        return view('admin.sinode.index')
            ->with('data', $data)
            ->with('title', 'Church Sinode');
    }

    public function create()
    {
        return view('admin.sinode.create')
            ->with('title', 'Church Sinode');
    }

    public function store(Request $request)
    {
        $data = Sinode::create([
            'name' => $request->name,
            'desc' => $request->desc,
            'created_by' => Auth::user()->name
        ]);

        return redirect()->route('churchsinode.index')->withSuccess('Sinode Created Successfully!');
    }

    public function edit($id)
    {
        $data = Sinode::find($id);

        return view('admin.sinode.edit')
            ->with('data', $data)
            ->with('title', 'Church Sinode');
    }

    public function update(Request $request, $id)
    {
        $data = Sinode::where('id', $id)
            ->update([
                'name' => $request->name,
                'sinode' => $request->sinode
            ]);

        return redirect()->route('churchsinode.index')->withSuccess('Sinode Updated Successfully!');
    }

    public function restore(Request $request)
    {
        $data = Sinode::withTrashed()->where('id', $request->id)->restore();

        return redirect()->route('churchsinode.index')->withSuccess('Sinode Restored Successfully!');
    }

    public function destroy($id)
    {
        $data = Sinode::where('id', $id)->delete();

        return redirect()->route('churchsinode.index')->withSuccess('Sinode Deleted Successfully!');
    }
}
