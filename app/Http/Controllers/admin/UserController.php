<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserRole;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->search;
        $data = User::withTrashed()->with('role')
            ->when($search, function ($query) use ($search) {
                return $query->where('name', 'like', '%' . $search . '%')
                    ->orWhere('created_by', 'like', '%' . $search . '%')
                    ->orWhere('username', 'like', '%' . $search . '%')
                    ->orWhere('email', 'like', '%' . $search . '%');
            })
            ->orderBy('name', 'asc')
            ->paginate(10);

        return view('admin.user.index')
            ->with('data', $data)
            ->with('title', 'User Data');
    }

    public function show($id)
    {
        $data = User::find($id);

        return view('admin.user.profile')
            ->with('data', $data)
            ->with('title', 'User Data');
    }

    public function create()
    {
        $role = UserRole::with('user')->orderBy('name')->get();

        return view('admin.user.create')
            ->with('role', $role)
            ->with('title', 'User Data');
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = User::create([
            'name' => $request->name,
            'role_id' => $request->role,
            'email' => $request->email,
            'username' => $request->username,
            'password' => bcrypt($request->password),
            'created_by' => Auth::user()->name,
        ]);

        return redirect()->route('userdata.index')->withSuccess('User Created Successfully!');
    }

    public function edit($id)
    {
        $data = User::find($id);
        $role = UserRole::with('user')->orderBy('name')->get();

        return view('admin.user.edit')
            ->with('data', $data)
            ->with('role', $role)
            ->with('title', 'User Data');
    }

    public function update(Request $request, $id)
    {
        $data = User::where('id', $id)
            ->update([
                'name' => $request->name,
                'role_id' => $request->role,
                'email' => $request->email,
                'username' => $request->username,
                'created_by' => Auth::user()->name
            ]);

        return redirect()->route('userdata.index')->withSuccess('User Updated Successfully!');
    }

    public function destroy($id)
    {
        $data = User::where('id', $id)->delete();

        return redirect()->route('userdata.index')->withSuccess('User Deleted Successfully!');
    }

    public function dashboard()
    {
        $user = User::with(['role', 'lastActivity'])->orderBy('created_at', 'desc')->get();
        $role = UserRole::with('user')->get();

        return view('admin.user.dashboard')
            ->with('user', $user)
            ->with('role', $role)
            ->with('title', 'User Dashboard');
    }

    public function verify(Request $request)
    {
        $data = User::where('id', $request->id)->update(['email_verified_at' => Carbon::now()]);

        return redirect()->route('userdata.index')->withSuccess('User Verified!');
    }

    public function restore(Request $request)
    {
        $data = User::withTrashed()->where('id', $request->id)->restore();

        return redirect()->route('userdata.index')->withSuccess('User Restored Successfully!');
    }

    public function reset(Request $request)
    {
        $id = $request->id;
        $email = $request->email;

        $data = User::where('id', $id)
            ->update([
                'password' => bcrypt($email),
            ]);

        return redirect()->route('userdata.index')->withSuccess('Password Reset Successfully!');
    }
}
