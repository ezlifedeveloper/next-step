<?php

namespace App\Http\Livewire;

use App\Helpers\Helper;
use App\Models\UserLog;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Login extends Component
{
    public $username, $password;

    public function render()
    {
        return view('livewire.login');
    }

    public function login()
    {
        if (Auth::attempt(['email' => $this->username, 'password' => $this->password]) || Auth::attempt(['username' => $this->username, 'password' => $this->password])) {
            Helper::recordUserLog(Auth::id(), 'Web Login');

            $this->alert('success', 'Login Berhasil', [
                'position' => 'top',
                'timer' =>  3000,
                'toast' => true
            ]);

            if(Auth::user()->role_id == 1) return redirect()->route('dashboard');
            else return redirect()->route('churchdashboard');
        } else {
            return $this->alert('error', 'Terjadi kesalahan dalam login. Cek kembali Username dan Password Anda', [
                'position' => 'top',
                'timer' =>  3000,
                'toast' => true
            ]);
        }
    }
}
