<?php

namespace App\Http\Livewire;

use App\Helpers\Helper;
use App\Jobs\GetDataFromSabda;
use App\Models\Bible;
use App\Models\BibleList;
use Livewire\Component;
use Livewire\WithPagination;

class BibleManagement extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $searchTerm;
    public $isEdit = 0;
    public $inputName, $inputDesc, $inputLanguage, $inputSource, $inputId;

    public function render()
    {
        $search = $this->searchTerm;

        return view('admin.bible.bible-management', [
            'list' => BibleList::selectRaw('language, count(id) as total')->groupBy('language')->get(),
            'data' => BibleList::with('bible')
                ->when($search, function ($query) use ($search) {
                    return $query->where('name', 'like', '%' . $search . '%')
                        ->orWhere('text', 'like', '%' . $search . '%')
                        ->orWhere('language', 'like', '%' . $search . '%');
                })
                ->orderBy('name')
                ->paginate(10)
        ])->extends('admin.app')
            ->layoutData([
                'title' => 'Bible Data',
            ]);
    }

    public function run($id, $version, $text)
    {
        if (GetDataFromSabda::dispatch($id, $version, $text))
            $this->alert('success', 'Data berhasil diambil');
        else $this->alert('danger', 'Data gagal diambil');
    }

    public function closeAll()
    {
        $this->isEdit = false;
    }

    public function create()
    {
        $this->isEdit = true;
    }

    public function edit($id)
    {
        $this->inputId = $id;

        $data = BibleList::find($id);
        $this->inputName = $data->name;
        $this->inputDesc = $data->text;
        $this->inputLanguage = $data->language;
        $this->inputSource = $data->source;

        $this->isEdit = true;
    }

    public function save()
    {
        $this->validate([
            'inputName' => 'required|string',
            'inputDesc' => 'required|string',
            'inputLanguage' => 'required|string',
            'inputSource' => 'required|string',
        ]);

        $data = BibleList::updateOrCreate(
            ['id' => $this->inputId],
            [
                'name' => $this->inputName,
                'text' => $this->inputDesc,
                'language' => $this->inputLanguage,
                'source' => $this->inputSource,
            ]
        );

        $this->alert('success', $this->inputId ? 'Data berhasil diperbarui' : 'Data berhasil disimpan');

        $this->reset(['inputName', 'inputId']);
        $this->isEdit = false;
    }

    public function delete($id)
    {
        $data = BibleList::where('id', $id)->delete();

        $this->alert('success', 'Data berhasil dihapus');
        $this->closeAll();
    }
}
