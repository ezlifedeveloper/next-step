<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        User::create([
            'name' => 'Prasetyo Nugrohadi',
            'username' => 'prasetyon',
            'email' => 'prasetyon@ezlife.id',
            'role_id' => 1,
            'password' => bcrypt('123')
        ]);

        User::create([
            'name' => 'Bagas Natan Mahendra',
            'username' => 'bagasnatan',
            'email' => 'bagasnatan@ezlife.id',
            'role_id' => 2,
            'password' => bcrypt('123')
        ]);
    }
}
