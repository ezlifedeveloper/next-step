<?php

namespace Database\Seeders;

use App\Models\Sinode;
use Illuminate\Database\Seeder;

class SinodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sinode::truncate();
        Sinode::create(['name' => 'GKI', 'desc' => 'Gereja Kristen Indonesia']);
        Sinode::create(['name' => 'GPIB', 'desc' => 'Gereja Protestan di Indonesia bagian Barat']);
        Sinode::create(['name' => 'GTI', 'desc' => 'Gereja Tiberias Indonesia']);
    }
}
