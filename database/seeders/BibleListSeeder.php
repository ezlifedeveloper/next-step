<?php

namespace Database\Seeders;

use App\Models\BibleList;
use Illuminate\Database\Seeder;

class BibleListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BibleList::truncate();
        BibleList::create(['name' => 'TB', 'text' => 'Terjemahan Baru', 'language' => 'Bahasa Indonesia', 'source' => 'sabda.org']);
        BibleList::create(['name' => 'TL', 'text' => 'Terjemahan Lama', 'language' => 'Bahasa Indonesia', 'source' => 'sabda.org']);
        // BibleList::create(['name' => 'NIV', 'text' => 'New International Version', 'language' => 'English']);
    }
}
