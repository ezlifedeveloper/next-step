<?php

namespace Database\Seeders;

use App\Models\UserRole;
use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserRole::truncate();
        UserRole::create(['name' => 'Superuser']);
        UserRole::create(['name' => 'Admin']);
        UserRole::create(['name' => 'User']);
    }
}
