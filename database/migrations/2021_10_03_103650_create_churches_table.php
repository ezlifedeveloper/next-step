<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChurchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('churches', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constraint('users')->nullable();
            $table->foreignId('sinode_id')->constraint('sinodes')->nullable();
            $table->string('name');
            $table->string('address');
            $table->string('phone')->nullable();
            $table->string('services')->nullable();
            $table->string('lat');
            $table->string('lng');
            $table->string('image')->nullable();
            $table->unsignedTinyInteger('status')->default(1);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('churches');
    }
}
