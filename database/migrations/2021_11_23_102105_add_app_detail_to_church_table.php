<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAppDetailToChurchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('churches', function (Blueprint $table) {
            $table->string('website')->nullable();
            $table->string('android')->nullable();
            $table->string('ios')->nullable();
            $table->boolean('app_showed')->default(false);
        });

        Schema::table('church_requests', function (Blueprint $table) {
            $table->string('website')->nullable();
            $table->string('android')->nullable();
            $table->string('ios')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('churches', function (Blueprint $table) {
            $table->dropColumn('website');
            $table->dropColumn('android');
            $table->dropColumn('ios');
            $table->dropColumn('app_showed');
        });

        Schema::table('church_requests', function (Blueprint $table) {
            $table->dropColumn('website');
            $table->dropColumn('android');
            $table->dropColumn('ios');
        });
    }
}
