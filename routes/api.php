<?php

use App\Http\Controllers\api\AuthController;
use App\Http\Controllers\api\VersionController;
use App\Http\Controllers\api\BibleController;
use App\Http\Controllers\api\ChurchController;
use App\Http\Controllers\api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    Route::prefix('check')->middleware('auth:api')->group(function () {
        Route::post('app', [VersionController::class, 'app']);
        Route::post('church', [VersionController::class, 'church']);
    });

    Route::prefix('bible')->middleware('auth:api')->group(function () {
        Route::get('version', [BibleController::class, 'version']);
        Route::post('data', [BibleController::class, 'get']);
    });

    Route::prefix('church')->middleware('auth:api')->group(function () {
        Route::get('sinode', [ChurchController::class, 'sinode']);
        Route::post('data', [ChurchController::class, 'get']);
        Route::post('request', [ChurchController::class, 'request']);
    });

    Route::prefix('auth')->group(function () {
        Route::post('login', [AuthController::class, 'login']);
        Route::post('google', [AuthController::class, 'google']);
        Route::post('logout', [AuthController::class, 'logout']);
    });

    Route::prefix('user')->middleware('auth:api')->group(function () {
        Route::post('list', [UserController::class, 'list']);
    });
});
