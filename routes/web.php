<?php

use App\Http\Controllers\admin\ChurchController;
use App\Http\Controllers\admin\ChurchRequestController;
use App\Http\Controllers\admin\ChurchSinodeController;
use App\Http\Controllers\admin\UserController;
use App\Http\Controllers\admin\UserRoleController;
use App\Http\Controllers\admin\VersionController;
use App\Http\Controllers\AuthController;
use App\Http\Livewire\BibleListManagement;
use App\Http\Livewire\BibleManagement;
use App\Http\Livewire\ChurchDashboard;
use App\Http\Livewire\ChurchManagement;
use App\Http\Livewire\ChurchRequestManagement;
use App\Http\Livewire\ChurchSinodeManagement;
use App\Http\Livewire\Dashboard;
use App\Http\Livewire\UserDashboard;
use App\Http\Livewire\UserManagement;
use App\Http\Livewire\UserRoleManagement;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'welcome')->name('home');

Route::view('login', 'web/login')->name('login');
Route::get('logout', [AuthController::class, 'logout'])->name('logout');

Route::get('test', function () {
    Storage::disk('google')->put('test.txt', 'Hello World');
});

Route::prefix('backoffice')->middleware('loggedin:1,2')->group(function () {
    Route::get('dashboard', Dashboard::class)->name('dashboard');
    Route::middleware('loggedin:1')->resource('version', VersionController::class)->names('version');

    Route::prefix('role')->middleware('loggedin:1')->group(function () {
        Route::resource('data', UserRoleController::class)->names('userrole');
        Route::post('restore', [UserRoleController::class, 'restore'])->name('userrole.restore');
        Route::post('delete', [UserRoleController::class, 'delete'])->name('userrole.delete');
    });

    Route::prefix('user')->middleware('loggedin:1')->group(function () {
        Route::get('dashboard', [UserController::class, 'dashboard'])->name('userdashboard');

        Route::resource('data', UserController::class)->names('userdata');
        Route::post('verify', [UserController::class, 'verify'])->name('userdata.verify');
        Route::post('reset', [UserController::class, 'reset'])->name('userdata.reset');
        Route::post('restore', [UserController::class, 'restore'])->name('userdata.restore');
    });


    Route::prefix('sinode')->middleware('loggedin:1,2')->group(function () {
        Route::resource('data', ChurchSinodeController::class)->names('churchsinode');
        Route::post('restore', [ChurchSinodeController::class, 'restore'])->name('churchsinode.restore');
    });

    Route::prefix('church')->middleware('loggedin:1,2')->group(function () {
        Route::get('dashboard', [ChurchController::class, 'dashboard'])->name('churchdashboard');

        Route::resource('data', ChurchController::class)->names('churchdata');
        Route::post('restore', [ChurchController::class, 'restore'])->name('churchdata.restore');
        Route::post('delete', [ChurchController::class, 'delete'])->name('churchdata.delete');

        Route::resource('request', ChurchRequestController::class)->names('churchrequest');
    });

    Route::prefix('bible')->middleware('loggedin:1')->group(function () {
        Route::get('data', BibleManagement::class)->name('bibledata');
    });
});
