@extends('admin.app')

@section('content')
<div>
    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-6">
                                    <a href=" {{ route('churchsinode.create') }}" class="btn btn-dark"><i class="fas fa-plus pr-1"></i> Add New</a>
                                </div>
                                <div class="col-6">
                                    <form method="GET">
                                        <div class="input-group">
                                            <input name="search" type="search" class="form-control form-control-lg" placeholder="Search Something">
                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-lg btn-default">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover m-0">
                                    <thead class="text-center">
                                        <tr>
                                            <th width="5%">#</th>
                                            <th class="text-left">NAME</th>
                                            <th class="text-left">CHURCH COUNT</th>
                                            <th class="text-left">CREATOR</th>
                                            <th width="10%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($data as $d)
                                        <tr>
                                            <td class="text-center">
                                                @if($d->trashed()) <i> @else <strong> @endif
                                                    {{ 10*($data->currentPage()-1)+$loop->iteration}}
                                                @if($d->trashed()) </i> @else </strong> @endif
                                            </td>
                                            <td>@if($d->trashed()) <i> @endif {{ $d->name }} @if(!$d->trashed()) <br/> @endif {{ $d->desc }} @if($d->trashed()) [deleted] </i> @endif</td>
                                            <td>@if($d->trashed()) <i> @endif {{ $d->church->count() }} @if($d->trashed()) </i> @endif</td>
                                            <td>@if($d->trashed()) <i> @endif {{ $d->created_by }} @if($d->trashed()) </i> @endif</td>
                                            <td style="text-align: center;">
                                                @if(!$d->trashed())
                                                <form method="post" id="{{'delete'.$d->id}}" action="{{ route('churchsinode.destroy', $d->id) }}">
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    @csrf
                                                </form>

                                                <a href="{{ route('churchsinode.edit', $d->id) }}" class="btn btn-sm btn-info" style="width:auto; margin: 2px"><i class="fas fa-edit"></i></a>
                                                <button form="{{'delete'.$d->id}}" type="submit" class="btn btn-sm btn-danger"
                                                    style="width:auto; margin: 2px" onclick="return confirm('Are you sure to delete?')">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                                @else
                                                <form method="post" id="{{'restore'.$d->id}}" action="{{ route('churchsinode.restore') }}">
                                                    <input name="id" type="hidden" value="{{$d->id}}">
                                                    @csrf
                                                </form>
                                                <button form="{{'restore'.$d->id}}" type="submit" class="btn btn-sm btn-success"
                                                    style="width:auto; margin: 2px" onclick="return confirm('Are you sure to restore?')">
                                                    <i class="fas fa-share"></i>
                                                </button>
                                                @endif
                                            </td>
                                        </tr>
                                        @empty
                                        <tr>
                                            <td colspan="5">No Data Available</td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                            @include('admin.layout.tablecountinfo')
                            <div class="text-xs" style="float: right">
                            @if($data->hasPages())
                                {{ $data->links() }}
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
			</div>
        </div>
    </div>
</div>
@endsection
