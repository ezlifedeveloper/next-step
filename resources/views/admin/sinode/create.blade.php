@extends('admin.app')

@section('content')
<div>
    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-6">
                                    <a href="{{ route('churchsinode.index') }}" class="btn btn-dark"><i class="fas fa-chevron-left pr-1"></i> Back</a>
                                </div>
                            </div>
                        </div>

                        <form method="post" action="{{route('churchsinode.store')}}">
                            @csrf
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="name">Abbr Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="name" id="name" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="desc">Full Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="desc" id="desc" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
			</div>
        </div>
    </div>
</div>
@endsection
