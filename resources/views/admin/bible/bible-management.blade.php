<div>
    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(!$isEdit)
                    <div class="card">
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover m-0">
                                    <thead class="text-center">
                                        <tr>
                                            <th width="5%">#</th>
                                            <th class="text-left">LANGUAGE</th>
                                            <th class="text-left">COUNT</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($list as $d)
                                        <tr>
                                            <td class="text-center"><strong>{{ $loop->iteration }}</strong></td>
                                            <td>{{ $d->language }}</td>
                                            <td>{{ $d->total }}</td>
                                        </tr>
                                        @empty
                                        <tr>
                                            <td colspan="5">No Data Available</td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-6">
                                    <button wire:click="create()" class="btn btn-dark"><i class="fas fa-plus pr-1"></i> Add New</button>
                                </div>
                                <div class="col-6">
                                    <input type="text" wire:model="searchTerm" placeholder="Search Something..." class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover m-0">
                                    <thead class="text-center">
                                        <tr>
                                            <th width="5%">#</th>
                                            <th class="text-left">VERSION</th>
                                            <th class="text-left">LANGUAGE</th>
                                            <th class="text-left">SOURCE</th>
                                            <th class="text-left">VERSE COUNT</th>
                                            <th width="10%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($data as $d)
                                        <tr>
                                            <td class="text-center"><strong>{{ 10*($data->currentPage()-1)+$loop->iteration}}</strong></td>
                                            <td>{{ $d->name }}<br/>{{ $d->text }}</td>
                                            <td>{{ $d->language }}</td>
                                            <td>{{ $d->source }}</td>
                                            <td>{{ $d->bible->count() }}</td>
                                            <td style="text-align: center;">
                                                <button wire:click.prevent="run({{ $d->id }}, '{{ $d->name }}', '{{ $d->text }}')"
                                                    wire:loading.class="disabled" class="btn btn-sm btn-success" style="width:auto; margin: 2px">
                                                    <span wire:loading wire:target="run({{ $d->id }}, '{{ $d->name }}', '{{ $d->text }}')" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                    <i wire:loading.remove wire:target="run({{ $d->id }}, '{{ $d->name }}', '{{ $d->text }}')" class="fas fa-paper-plane"></i>
                                                </button>
                                                <button wire:click="edit({{ $d->id }})" class="btn btn-sm btn-info" style="width:auto; margin: 2px"><i class="fas fa-edit"></i></button>
                                                <button wire:click="delete({{ $d->id }})" class="btn btn-sm btn-danger" style="width:auto; margin: 2px" onclick="confirm('Are you sure to delete?') || event.stopImmediatePropagation()"><i class="fas fa-trash"></i></button>
                                            </td>
                                        </tr>
                                        @empty
                                        <tr>
                                            <td colspan="5">No Data Available</td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                            @include('admin.layout.tablecountinfo')
                            <div class="text-xs" style="float: right">
                            @if($data->hasPages())
                                {{ $data->links() }}
                            @endif
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-6">
                                    <button wire:click="closeAll()" class="btn btn-dark"><i class="fas fa-chevron-left pr-1"></i> Back</button>
                                </div>
                            </div>
                        </div>
                        <form wire:submit.prevent="save()">
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="inputName">Version Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" wire:model="inputName" id="inputName" class="form-control" required>
                                        @error('inputName') <div class="invalid-feedback">{{ $message }}</div> @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="inputDesc">Full Version Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" wire:model="inputDesc" id="inputDesc" class="form-control" required>
                                        @error('inputDesc') <div class="invalid-feedback">{{ $message }}</div> @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="inputLanguage">Language</label>
                                    <div class="col-sm-10">
                                        <input type="text" wire:model="inputLanguage" id="inputLanguage" class="form-control" required>
                                        @error('inputLanguage') <div class="invalid-feedback">{{ $message }}</div> @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="inputSource">Source</label>
                                    <div class="col-sm-10">
                                        <input type="text" wire:model="inputSource" id="inputSource" class="form-control" required>
                                        @error('inputSource') <div class="invalid-feedback">{{ $message }}</div> @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                    @endif
                </div>
			</div>
        </div>
    </div>
</div>
