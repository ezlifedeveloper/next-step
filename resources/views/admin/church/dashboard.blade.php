@extends('admin.app')

@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Content Row -->
    <div class="row">
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="small-box bg-info mb-3">
                <div class="inner">
                    <h3>{{$data['church_count']}}</h3>

                    <p>Church Registered</p>
                </div>
                <div class="icon">
                    <i class="fas fa-church"></i>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-md-6 mb-4">
            <div class="small-box bg-danger mb-3">
                <div class="inner">
                    <h3>JPCC Kota Kasablanka</h3>

                    <p>Most Visited Church</p>
                </div>
                <div class="icon">
                    <i class="fas fa-church"></i>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-md-6 mb-4">
            <div class="small-box bg-primary mb-3">
                <div class="inner">
                    <h3>{{$data['visit_count']}}</h3>

                    <p>Church Visited</p>
                </div>
                <div class="icon">
                    <i class="fas fa-church"></i>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-md-6 mb-4">
            <div class="small-box bg-warning mb-3">
                <div class="inner">
                    <h3>{{$data['sinode_count']}}</h3>

                    <p>Sinode</p>
                </div>
                <div class="icon">
                    <i class="fas fa-database"></i>
                </div>
            </div>
        </div>
    </div>

    <!-- Content Row -->

    <div class="row">
        <!-- Area Chart -->
        <div class="col-xl-12 col-lg-12">
            <div class="card shadow mb-4 p-0">
                <div class="card-body p-1">
                    <div class="col-lg-12">
                        <div  style="height: 100%; width:100%">
                            <div id="map" style="height: 520px"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <!-- Area Chart -->
        <div class="col-xl-12 col-lg-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header bg-success">
                    <h3 class="card-title">Recently Added</h3>
                </div>

                <!-- Card Body -->
                <div class="card-body">
                    <div class="row">
                    @foreach($data['church_data'] as $data)
                        <div class="col-lg-2 col-md-3 col-sm-12" >
                            <div style="text-align:center;">
                                @php $image = $data->image ? $data->image : env('APP_URL').'/msn/public/template/imgnotavail.jpeg'; @endphp
                                <img src="{{$image}}" style="width: 150px; height:150px;"/>
                                <p style="width:100%;display: block; margin-left: auto; margin-right: auto;">
                                {{$data->name}} <br>
                                {{'Created on '.$data->created_at}}
                                </p>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->

@endsection

@section('script')

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBxQfd0X4Ai62z50ckEq-vnxtKjW7S1rcE&callback=initMap" defer></script>
<script src="https://unpkg.com/@googlemaps/markerclustererplus/dist/index.min.js"></script>

<script>
    const locations = @php echo json_encode($church); @endphp;

    function initMap() {
        map = new google.maps.Map(document.getElementById("map"), {
            center: { lat: -0.5, lng: 120.572597 },
            zoom: 5,
        });

        //Create and open InfoWindow.
        var infoWindow = new google.maps.InfoWindow();

        const labels = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var markers = [];

        for (var i = 0; i < locations.length; i++) {
            var data = locations[i]['label'];
            var myLatlng = new google.maps.LatLng(locations[i]['position']);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: data
            });

            //Attach click event to the marker.
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    //Wrap the content inside an HTML DIV in order to set height and width of InfoWindow.
                    infoWindow.setContent("<div style = 'width:200px;min-height:40px'>" + data + "</div>");
                    infoWindow.open(map, marker);
                });
            })(marker, data);

            markers.push(marker);
        }

        // var markers = locations.map((location, i) => {
        //     return new google.maps.Marker({
        //         position: location['position'],
        //         label: labels[i % labels.length],
        //     });
        // });
        // Add a marker clusterer to manage the markers.
        new MarkerClusterer(map, markers, {
            imagePath:
            "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m",
        });
    }
</script>
@endsection
