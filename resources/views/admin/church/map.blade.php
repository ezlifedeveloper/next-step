@section('style')

<style>
    /* Always set the map height explicitly to define the size of the div
    * element that contains the map. */
    #map {
      height: 400px;
      width: 100%;
    }

    #infowindow-content .title {
      font-weight: bold;
    }

    #infowindow-content {
      display: none;
    }

    #map #infowindow-content {
      display: inline;
    }

    .input-index {
      z-index: 10000 !important;
      padding-bottom: 12px;
      margin-right: 12px;
    }

    .pac-container {
      z-index: 10000 !important;
      padding-bottom: 12px;
      margin-right: 12px;
    }

    .pac-controls label {
      font-family: Roboto;
      font-size: 13px;
      font-weight: 300;
    }

    #pac-input {
      background-color: #fff;
      font-family: Roboto;
      font-size: 15px;
      font-weight: 300;
      padding: 0 11px 0 13px;
      text-overflow: ellipsis;
      width: 100%;
    }

    #pac-input:focus {
      border-color: #4d90fe;
    }
</style>


<!-- Maps Modal -->
<div class="modal inmodal fade" id="alamat-map" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Find Address</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">
                @method('PATCH')
                @csrf
                <div class="form-group">
                    <label class="col-sm-2 control-label" style="text-align: left">Search Location</label>
                    <div class="col-sm-10"><input type="text" class="form-control input-index" name="pac-input" id="pac-input"></div>
                </div>

                <div id="map"></div>
                <div id="infowindow-content">
                    <img src="" width="16" height="16" id="place-icon">
                    <span id="place-name"  class="title"></span><br>
                    <span id="place-address"></span>
                </div>

                <div class="form-group" style="margin-top: 15px">
                    <label class="col-sm-2 control-label" style="text-align: left">Latitude</label>
                    <div class="col-sm-10"><input type="text" class="form-control" name="latitude" id="latitude" readonly></div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" style="text-align: left">Longitude</label>
                    <div class="col-sm-10"><input type="text" class="form-control" name="longitude" id="longitude" readonly></div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" style="text-align: left">Address</label>
                    <div class="col-sm-10"><input type="text" class="form-control" name="address" id="address" readonly></div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBxQfd0X4Ai62z50ckEq-vnxtKjW7S1rcE&libraries=places&callback=initAllMap" async defer></script>
    <script>
        $('.map').on('click',function(){
            $('#alamat-map').modal('show');
        })

        function setAddress(idLatitude, idLongitude, idAlamat, id_latitude, id_longitude, id_alamat, lat, lng, address) {
            document.getElementById("latitude").value = lat;
            document.getElementById("longitude").value = lng;
            document.getElementById("address").value = address;
            document.getElementById("inputLatitude").value = lat;
            document.getElementById("inputLongitude").value = lng;
            document.getElementById("inputAlamat").value = address;

            $('#'+idLatitude).trigger('change');
            $('#'+idLongitude).trigger('change');
            $('#'+idAlamat).trigger('change');

            $('#'+id_latitude).trigger('change');
            $('#'+id_longitude).trigger('change');
            $('#'+id_alamat).trigger('change');
        }

        function initMap(idMap, idInfoWindow, idPacInput, idPlaceIcon, idPlaceName, idPlaceAddress, idLatitude, idLongitude, idAlamat, _idLintang, _idBujur, _idAlamat) {
        var map = new google.maps.Map(document.getElementById(idMap), {
            center: {lat: -7.283991, lng: 112.759684},
            zoom: 13
        });

        var input = document.getElementById(idPacInput);

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        autocomplete.setFields(
                ['address_components', 'geometry', 'icon', 'name']);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById(idInfoWindow);
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
            window.alert("No details available for input: '" + place.name + "'");
            return;
            }

            if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
            } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var geocoder = new google.maps.Geocoder;
            var lat = place.geometry.location.lat();
            var lng = place.geometry.location.lng();
            var newPos = new google.maps.LatLng(lat, lng);
            var address = '';

            geocoder.geocode({'location': newPos}, function (result, status) {
            if (status === 'OK') {
                if (result[0]) {
                address = [result[0].formatted_address];
                infowindowContent.children[idPlaceIcon].src = place.icon;
                infowindowContent.children[idPlaceName].textContent = place.name;
                infowindowContent.children[idPlaceAddress].textContent = address;
                infowindow.open(map, marker);
                setAddress(idLatitude, idLongitude, idAlamat, _idLintang, _idBujur, _idAlamat, lat, lng, address);
                }
            }
            });
        });

        google.maps.event.addListener(map, 'click', function (event) {
            var lat = event.latLng.lat();
            var lng = event.latLng.lng();
            var newPos = new google.maps.LatLng(lat, lng);
            var geocoder = new google.maps.Geocoder;

            marker.setVisible(false);
            map.setCenter(newPos);
            map.setZoom(17);
            marker.setPosition(newPos);
            marker.setVisible(true);
            var address = '';

            geocoder.geocode({'location': newPos}, function (result, status) {
            if (status === 'OK') {
                if (result[0]) {
                console.log(lat);
                address = result[0].formatted_address;
                setAddress(idLatitude, idLongitude, idAlamat, _idLintang, _idBujur, _idAlamat, lat, lng, address);
                }
            }
            });
        });
        }

        function initAllMap() {
            initMap('map', 'infowindow-content', 'pac-input', 'place-icon', 'place-name', 'place-address', 'latitude', 'longitude', 'address', 'latitude', 'longitude', 'alamat');
        }
    </script>
@stop
