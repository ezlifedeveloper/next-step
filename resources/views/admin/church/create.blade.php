@extends('admin.app')
@include('admin.church.map')

@section('content')
<div>
    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-6">
                                    <a href="{{ route('churchdata.index') }}" class="btn btn-dark"><i
                                            class="fas fa-chevron-left pr-1"></i> Back</a>
                                </div>
                            </div>
                        </div>

                        <form method="post" action="{{route('churchdata.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="name">Church Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" id="inputNama" class="form-control" name="name"
                                            placeholder="GKI Harapan Indah" required autofocus="autofocus">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="desc">Sinode</label>
                                    <div class="col-sm-10">
                                        <select name="sinode" class="form-control select2" required>
                                            <option value="" selected="selected">-- Select Sinode --</option>
                                            @foreach($sinode as $t)
                                            <option value="{{ $t->id }}">{{ $t->name.' | '.$t->desc }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="phone">Phone Number</label>
                                    <div class="col-sm-10">
                                        <input type="text" id="inputPhone" class="form-control" name="phone"
                                            placeholder="+6281234567890">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="website">Website URL</label>
                                    <div class="col-sm-10">
                                        <input type="text" id="inputwebsite" class="form-control" name="website"
                                            placeholder="https://blablabla.id">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="android">Play Store URL</label>
                                    <div class="col-sm-10">
                                        <input type="text" id="inputAndroid" class="form-control" name="android"
                                            placeholder="https://blablabla.id">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="ios">App Store URL</label>
                                    <div class="col-sm-10">
                                        <input type="text" id="inputios" class="form-control" name="ios"
                                            placeholder="https://blablabla.id">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="phone">Service Hours (semicolon
                                        separator)</label>
                                    <div class="col-sm-10">
                                        <input type="text" id="inputService" class="form-control" name="service"
                                            placeholder="07:00; 09:00; 17:00">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="address">Address</label>
                                    <div class="col-sm-10">
                                        <input type="text" id="inputAlamat" class="form-control map"
                                            placeholder="Jl. Jalan dari sana ke sini 12 kali, 10101" name="address">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="latitude">Latitude</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control map" placeholder="-123,32921"
                                            id="inputLatitude" name="latitude" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="longitude">Longitude</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control map" placeholder="129,0123912"
                                            id="inputLongitude" name="longitude" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="file">Image</label>
                                    <div class="col-sm-10">
                                        <input type="file" id="inputFile" accept="image/*" name="file"
                                            placeholder="Upload" style="padding-top: 5px"
                                            onchange="previewImage(this);">
                                        <br /><br />
                                        <img id="imgpreview" src="#" alt="your image" width="500px" />
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
