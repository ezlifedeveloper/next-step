@extends('admin.app')

@section('content')
<div>
    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-6">
                                    {{-- <a href=" {{ route('churchdata.create') }}" class="btn btn-dark"><i class="fas fa-plus pr-1"></i> Add New</a> --}}
                                </div>
                                <div class="col-6">
                                    <form method="GET">
                                        <div class="input-group">
                                            <input name="search" type="search" class="form-control form-control-lg" placeholder="Search Something">
                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-lg btn-default">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover m-0">
                                    <thead class="text-center">
                                        <tr>
                                            <th width="5%">#</th>
                                            <th class="text-left">STATUS</th>
                                            <th class="text-left">NAME</th>
                                            <th class="text-left">SINODE</th>
                                            <th class="text-left">COORDINATE</th>
                                            <th class="text-left">ADDRESS</th>
                                            <th class="text-left">SERVICES</th>
                                            <th class="text-left">IMAGE</th>
                                            <th class="text-left">ISSUED BY</th>
                                            <th width="10%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($data as $d)
                                        <tr>
                                            <td class="text-center">{{ 10*($data->currentPage()-1)+$loop->iteration}} </td>
                                            <td>
                                                @if($d->church_id) Suggest Update
                                                @else New Entry
                                                @endif
                                                <br/>
                                                @if($d->status == 1) <span class="badge badge-success"> Approved </span>
                                                @elseif($d->status == 2) <span class="badge badge-danger"> Rejected </span>
                                                @else <span class="badge badge-primary"> Waiting for Approval </span>
                                                @endif
                                            </td>
                                            <td>
                                                @if($d->name)
                                                    @if($d->parent && $d->parent->name!=$d->name) {{ $d->parent->name}} <br/> to <br/> {{ $d->name }}
                                                    @elseif(!$d->parent) {{ $d->name }}
                                                    @endif
                                                @endif
                                            </td>
                                            <td>
                                                @if($d->sinode_id)
                                                    @if($d->parent && $d->parent->sinode_id!=$d->sinode_id) {{ $d->parent->sinode->desc}} <br/> to <br/> {{ $d->sinode->desc }}
                                                    @elseif(!$d->parent) {{ $d->sinode->desc }}
                                                    @endif
                                                @endif
                                            </td>
                                            <td>
                                                @if($d->lat)
                                                    @if($d->parent && $d->parent->lat!=$d->lat) {{ $d->parent->lat.', '.$d->parent->lng }}  <br/> to <br/> {{ $d->lat.', '.$d->lng }}
                                                    @elseif(!$d->parent) {{ $d->lat.', '.$d->lng }}
                                                    @endif
                                                @endif
                                            </td>
                                            <td>
                                                @if($d->address)
                                                    @if($d->parent && $d->parent->address!=$d->address) {{ $d->parent->address}} <br/> to <br/> {{ $d->address }}
                                                    @elseif(!$d->parent) {{ $d->address }}
                                                    @endif
                                                @endif
                                            </td>
                                            <td>
                                                @if($d->services)
                                                    @if($d->parent && $d->parent->services!=$d->services) {{ $d->parent->name}} <br/> to <br/> {{ $d->services }}
                                                    @elseif(!$d->parent) {{ $d->services }}
                                                    @endif
                                                @endif
                                            </td>
                                            <td>
                                                @if($d->image)
                                                @if($d->parent) <img src="{{ $d->parent->image }}" width='100px' height='100px'> <br/> to <br/> @endif
                                                <img src="{{ $d->image }}" width='100px' height='100px'>
                                                @endif
                                            </td>
                                            <td>{{ $d->user_id ? $d->user->name : '-' }}</td>
                                            <td style="text-align: center;">
                                                @if($d->status==0)
                                                <form method="post" id="{{'confirm'.$d->id}}" action="{{ route('churchrequest.update', $d->id) }}">
                                                    <input name="status" type="hidden" value="1">
                                                    @method('put')
                                                    @csrf
                                                </form>
                                                <form method="post" id="{{'reject'.$d->id}}" action="{{ route('churchrequest.update', $d->id) }}">
                                                    <input name="status" type="hidden" value="2">
                                                    @method('put')
                                                    @csrf
                                                </form>
                                                <button form="{{'confirm'.$d->id}}" type="submit" class="btn btn-sm btn-success"
                                                    style="width:auto; margin: 2px" onclick="return confirm('Are you sure to confirm?')">
                                                    <i class="fas fa-check"></i>
                                                </button>
                                                <button form="{{'reject'.$d->id}}" type="submit" class="btn btn-sm btn-warning"
                                                    style="width:auto; margin: 2px" onclick="return confirm('Are you sure to reject?')">
                                                    <i class="fas fa-times"></i>
                                                </button>
                                                @endif

                                                <form method="post" id="{{'delete'.$d->id}}" action="{{ route('churchrequest.destroy', $d->id) }}">
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    @csrf
                                                </form>

                                                @if(in_array(Auth::user()->role_id, [1]))
                                                <button form="{{'delete'.$d->id}}" type="submit" class="btn btn-sm btn-danger"
                                                    style="width:auto; margin: 2px" onclick="return confirm('Are you sure to delete?')">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                                @endif
                                            </td>
                                        </tr>
                                        @empty
                                        <tr>
                                            <td colspan="7">No Data Available</td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                            @include('admin.layout.tablecountinfo')
                            <div class="text-xs" style="float: right">
                            @if($data->hasPages())
                                {{ $data->links() }}
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
			</div>
        </div>
    </div>
</div>
@endsection
