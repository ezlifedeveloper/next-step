@extends('admin.app')

@section('content')
<div>
    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-2 col-sm-3">
                    <!-- small box -->
                    <div class="small-box bg-info mb-3">
                        <div class="inner">
                            <h3>{{ $user->count() }}</h3>

                            <p>Total User</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-user"></i>
                        </div>
                        <a href="{{ route('userdata.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>

                    <!-- RECENT USER LIST -->
                    <div class="card">
                        <div class="card-header bg-success">
                            <h3 class="card-title">Recent User</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0">
                            <ul class="products-list product-list-in-card pl-2 pr-2">
                                @foreach ($user->take(5) as $d)
                                <li class="item">
                                    <div class="product-img">
                                        <img src="{{ $d->photo ?? asset('ppplaceholder.png') }}" alt="Product Image" class="img-size-50">
                                    </div>
                                    <div class="product-info">
                                        <p class="product-title mb-0">{{ $d->name }}</p>
                                        {{-- <span class="badge badge-warning float-right">$1800</span></a> --}}
                                        <span class="product-description">
                                        {{ $d->created_at->diffForHumans() }}
                                        </span>
                                    </div>
                                </li>
                                <!-- /.item -->
                                @endforeach
                            </ul>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                    <!-- ROLE LIST -->
                    <div class="card">
                        <div class="card-header bg-danger">
                            <h3 class="card-title">Role</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0">
                            <ul class="products-list product-list-in-card pl-2 pr-2">
                                @foreach ($role as $d)
                                <li class="item pl-2 pr-2">
                                    <div>
                                        <p class="product-title mb-0">{{ $d->name }}<span class="badge badge-warning float-right">{{$d->user->count()}}</span></p>
                                    </div>
                                </li>
                                <!-- /.item -->
                                @endforeach
                            </ul>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <div class="col-lg-10 col-sm-9">
                    <!-- USERS LIST -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Most Point</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0">
                            <div class="row p-3">
                                @foreach ($user->sortByDesc('point')->take(6) as $d)
                                <div class="col-md-2 col-sm-4 col-xs-6 text-center">
                                    <img class="img-size-64" src="{{ $d->photo ?? asset('ppplaceholder.png') }}" alt="{{ $d->name }}">
                                    <a class="users-list-name" href="#">{{ $d->name }}</a>
                                    <span class="users-list-date">{{ $d->point.' pts' }}</span>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!--/.card -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
