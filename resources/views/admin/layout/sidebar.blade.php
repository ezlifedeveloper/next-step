<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('home') }}" class="brand-link">
        <img src="{{ asset('logo.jpg') }}" alt="{{ env('APP_NAME') }} Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">{{ env('APP_NAME') }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- SidebarSearch Form -->
        <div class="form-inline mt-2">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                @if(in_array(Auth::user()->role_id, [1]))
                <li class="nav-header">Dashboard</li>
                <li class="nav-item">
                    <a href="{{ route('dashboard') }}" class="nav-link @if(in_array($title, ['Dashboard'])) active @endif">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                            {{-- <span class="right badge badge-danger">New</span> --}}
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('version.index') }}" class="nav-link @if(in_array($title, ['App Version'])) active @endif">
                        <i class="nav-icon fa fa-history"></i>
                        <p>
                            App Version
                            {{-- <span class="right badge badge-danger">New</span> --}}
                        </p>
                    </a>
                </li>
                @endif
                @if(in_array(Auth::user()->role_id, [1, 2]))
                <li class="nav-header">CHURCH</li>
                <li class="nav-item">
                        <a href="{{ route('churchdashboard') }}" class="nav-link @if(in_array($title, ['Church Dashboard'])) active @endif">
                        <i class="fas fa-tachometer-alt nav-icon"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('churchsinode.index') }}" class="nav-link @if(in_array($title, ['Church Sinode'])) active @endif">
                        <i class="fas fa-database nav-icon"></i>
                        <p>Sinode</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('churchdata.index') }}" class="nav-link @if(in_array($title, ['Church Data'])) active @endif">
                        <i class="fas fa-church nav-icon"></i>
                        <p>Data</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('churchrequest.index') }}" class="nav-link @if(in_array($title, ['Church Request'])) active @endif">
                        <i class="fas fa-folder-open nav-icon"></i>
                        <p>Request</p>
                    </a>
                </li>
                @endif
                @if(Auth::user()->role_id == 1)
                <li class="nav-header">BIBLE</li>
                <li class="nav-item">
                    <a href="{{ route('bibledata') }}" class="nav-link @if(in_array($title, ['Bible Data'])) active @endif">
                        <i class="fas fa-book nav-icon"></i>
                        <p>Data</p>
                    </a>
                </li>
                @endif
                @if(Auth::user()->role_id == 1)
                <li class="nav-header">USER</li>
                <li class="nav-item">
                        <a href="{{ route('userdashboard') }}" class="nav-link @if(in_array($title, ['User Dashboard'])) active @endif">
                        <i class="fas fa-tachometer-alt nav-icon"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('userrole.index') }}" class="nav-link @if(in_array($title, ['User Role'])) active @endif">
                        <i class="fas fa-database nav-icon"></i>
                        <p>Role</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('userdata.index') }}" class="nav-link @if(in_array($title, ['User Data'])) active @endif">
                        <i class="fas fa-user nav-icon"></i>
                        <p>Data</p>
                    </a>
                </li>
                @endif
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
