<footer class="main-footer">
    <strong><a href="https://ezlife.id">EZ-Life Developer</a> &copy; 2017 - {{date('Y')}}</strong>
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 1.0.0
    </div>
</footer>
