<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="shortcut icon" type="image/png" href="{{ asset('favicon.ico') }}">

<meta name="description" content="EZ-Life adalah pengembang aplikasi website, android, ios, maupun desktop yang mengutamakan kenyamanan client, ketepatan waktu, dan hasil yang maksimal dan optimal">
<meta name="author" content="EZ-Life Developer">
<meta property="og:description" content="EZ-Life adalah pengembang aplikasi website, android, ios, maupun desktop" />
<meta property="og:url" content="{{ env("APP_URL") }}" />
<meta property="og:image" content="{{ asset('logo.jpg') }}" />
<meta property="og:title" content="{{ env("APP_NAME") }}" />
<meta property="og:type" content="portfolio" />
<meta property="og:locale:alternate" content="in_ID" />

<meta name="twitter:card" content="summary" />
{{-- <meta name="twitter:site" content="@ezlifedeveloper" />
<meta name="twitter:creator" content="@ezlifedeveloper" /> --}}
