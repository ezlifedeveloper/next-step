<!DOCTYPE html>
<html lang="en">
<head>
    @include('admin.layout.head')

    <title>{{$title}} | {{env('APP_NAME')}}</title>
    @include('admin.layout.css')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        <!-- Preloader -->
        <div class="preloader flex-column justify-content-center align-items-center">
            <img class="animation__shake" src="{{ asset('logo.jpg') }}" alt="Logo" height="60" width="60">
        </div>

        @include('admin.layout.nav')
        @include('admin.layout.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">

            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                @yield('content')
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        @include('admin.layout.footer')
        @include('admin.layout.sidebox')
    </div>
    <!-- ./wrapper -->

    @include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])
    @include('admin.layout.js')
</body>
</html>
