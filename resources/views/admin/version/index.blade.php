@extends('admin.app')

@section('content')
<div>
    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-6">
                                    <a href=" {{ route('version.create') }}" class="btn btn-dark"><i class="fas fa-plus pr-1"></i> Add New</a>
                                </div>
                                <div class="col-6">
                                    <form method="GET">
                                        <div class="input-group">
                                            <input name="search" type="search" class="form-control form-control-lg" placeholder="Search Something">
                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-lg btn-default">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover m-0">
                                    <thead class="text-center">
                                        <tr>
                                            <th width="5%">#</th>
                                            <th class="text-left">VERSION</th>
                                            <th class="text-left">TYPE</th>
                                            <th class="text-left">DATE</th>
                                            <th class="text-left">DESCRIPTION</th>
                                            <th class="text-left">CREATOR</th>
                                            <th width="10%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($data as $d)
                                        <tr>
                                            <td class="text-center">{{ 10*($data->currentPage()-1)+$loop->iteration}}</td>
                                            <td>{{ $d->version }}</td>
                                            <td>{{ $d->platform == 1 ? 'Mobile App' : 'Website' }}</td>
                                            <td>{{ $d->created_at }}</td>
                                            <td style="white-space: pre-line;">{{ $d->desc }}</td>
                                            <td>{{ $d->creator->name }}</td>
                                            <td style="text-align: center;">
                                                <form method="post" id="{{'delete'.$d->id}}" action="{{ route('version.destroy', $d->id) }}" onsubmit="return confirm('Are you sure to delete?');">
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    @csrf
                                                </form>

                                                <a href="{{ route('version.edit', $d->id) }}" class="btn btn-sm btn-info" style="width:auto; margin: 2px"><i class="fas fa-edit"></i></a>
                                                <button form="{{'delete'.$d->id}}" type="submit" class="btn btn-sm btn-danger" style="width:auto; margin: 2px">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        @empty
                                        <tr>
                                            <td colspan="7">No Data Available</td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                            @include('admin.layout.tablecountinfo')
                            <div class="text-xs" style="float: right">
                            @if($data->hasPages())
                                {{ $data->links() }}
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
			</div>
        </div>
    </div>
</div>
@endsection
