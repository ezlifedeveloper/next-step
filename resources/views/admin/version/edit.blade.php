@extends('admin.app')

@section('content')
<div>
    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-6">
                                    <a href="{{ route('version.index') }}" class="btn btn-dark"><i class="fas fa-chevron-left pr-1"></i> Back</a>
                                </div>
                            </div>
                        </div>

                        <form method="post" action="{{route('version.update', $data->id)}}">
                            @csrf
                            @method('put')
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="version">Version Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="version" id="version" class="form-control" value="{{$data->version}}" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="platform">Platform</label>
                                    <div class="col-sm-10">
                                        <select name="platform" class="form-control" required>
                                            <option value="" selected="selected">-- Select Platform --</option>
                                            <option value="0" @if($data->platform==0) selected @endif>Website</option>
                                            <option value="1" @if($data->platform==1) selected @endif>Mobile App</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="date">Released Date</label>
                                    <div class="col-sm-10">
                                        <input type="date" class="form-control" name="date" required="required" value="{{$data->date}}"
                                        data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="date">Description</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="desc" rows="7">{{$data->desc}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
			</div>
        </div>
    </div>
</div>
@endsection
