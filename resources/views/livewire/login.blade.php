<div>
    <form wire:submit.prevent="login()">
        <div class="input-group mb-3">
            <input type="text" wire:model="username" class="form-control" placeholder="Username / Email">
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-user"></span>
                </div>
            </div>
        </div>
        <div class="input-group mb-3">
            <input type="password"wire:model="password"  class="form-control" placeholder="Password">
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <button type="submit" class="btn btn-primary btn-block">
                    <div wire:loading wire:target="login">
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        Logging In....
                    </div>
                    <div wire:loading.remove wire:target="login">Log In</div>
                </button>
            </div>
        </div>
    </form>
</div>
