<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Next Step API</title>

    <link href="https://fonts.googleapis.com/css?family=PT+Sans&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="{{ asset("vendor/scribe/css/style.css") }}" media="screen" />
        <link rel="stylesheet" href="{{ asset("vendor/scribe/css/print.css") }}" media="print" />
        <script src="{{ asset("vendor/scribe/js/all.js") }}"></script>

        <link rel="stylesheet" href="{{ asset("vendor/scribe/css/highlight-darcula.css") }}" media="" />
        <script src="{{ asset("vendor/scribe/js/highlight.pack.js") }}"></script>
    <script>hljs.initHighlightingOnLoad();</script>

</head>

<body class="" data-languages="[&quot;bash&quot;,&quot;javascript&quot;]">
<a href="#" id="nav-button">
      <span>
        NAV
            <img src="{{ asset("vendor/scribe/images/navbar.png") }}" alt="-image" class=""/>
      </span>
</a>
<div class="tocify-wrapper">
                <div class="lang-selector">
                            <a href="#" data-language-name="bash">bash</a>
                            <a href="#" data-language-name="javascript">javascript</a>
                    </div>
        <div class="search">
        <input type="text" class="search" id="input-search" placeholder="Search">
    </div>
    <ul class="search-results"></ul>

    <ul id="toc">
    </ul>

            <ul class="toc-footer" id="toc-footer">
                            <li><a href="{{ route("scribe.postman") }}">View Postman collection</a></li>
                            <li><a href="{{ route("scribe.openapi") }}">View OpenAPI (Swagger) spec</a></li>
                            <li><a href='http://github.com/knuckleswtf/scribe'>Documentation powered by Scribe ✍</a></li>
                    </ul>
            <ul class="toc-footer" id="last-updated">
            <li>Last updated: November 2 2021</li>
        </ul>
</div>
<div class="page-wrapper">
    <div class="dark-box"></div>
    <div class="content">
        <h1>Introduction</h1>
<p>API documentation for Next Step Application</p>
<p>This documentation aims to provide all the information you need to work with our API.</p>
<aside>As you scroll, you'll see code examples for working with the API in different programming languages in the dark area to the right (or as part of the content on mobile).
You can switch the language used with the tabs at the top right (or from the nav menu at the top left on mobile).</aside>
<script src="https://cdn.jsdelivr.net/npm/lodash@4.17.10/lodash.min.js"></script>
<script>
    var baseUrl = "http://localhost:8000";
</script>
<script src="{{ asset("vendor/scribe/js/tryitout-2.7.10.js") }}"></script>
<blockquote>
<p>Base URL</p>
</blockquote>
<pre><code class="language-yaml">http://localhost:8000</code></pre><h1>Authenticating requests</h1>
<p>To authenticate requests, include a query parameter <strong><code>api_token</code></strong> in the request.</p>
<p>All authenticated endpoints are marked with a <code>requires authentication</code> badge in the documentation below.</p>
<p>You can retrieve your token by logging in</p><h1>App Version Management</h1>
<p>APIs for checking application and data version</p>
<h2>Get Latest App Version Info</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Get Latest App Version Info, on success you'll get a 200 OK response.
Return 422 when parameter is not filled.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost:8000/api/v1/check/app?api_token=%7BYOUR_AUTH_KEY%7D" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"platform":1}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/check/app"
);

let params = {
    "api_token": "{YOUR_AUTH_KEY}",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "platform": 1
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<div id="execution-results-POSTapi-v1-check-app" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-v1-check-app"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-check-app"></code></pre>
</div>
<div id="execution-error-POSTapi-v1-check-app" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-check-app"></code></pre>
</div>
<form id="form-POSTapi-v1-check-app" data-method="POST" data-path="api/v1/check/app" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-check-app', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-v1-check-app" onclick="tryItOut('POSTapi-v1-check-app');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-v1-check-app" onclick="cancelTryOut('POSTapi-v1-check-app');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-v1-check-app" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/v1/check/app</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>api_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="api_token" data-endpoint="POSTapi-v1-check-app" data-component="query" required  hidden>
<br>
Authentication key.
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>platform</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="platform" data-endpoint="POSTapi-v1-check-app" data-component="body" required  hidden>
<br>
Platform to be checked (0: Website, 1: Mobile).
</p>

</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Latest Version data.
</p>
<h2>Get Latest Church Timestamp</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Get Latest Church Timestamp, on success you'll get a 200 OK response.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost:8000/api/v1/check/church?api_token=%7BYOUR_AUTH_KEY%7D" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"date":"2021-10-13 11:00:00"}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/check/church"
);

let params = {
    "api_token": "{YOUR_AUTH_KEY}",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "date": "2021-10-13 11:00:00"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<div id="execution-results-POSTapi-v1-check-church" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-v1-check-church"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-check-church"></code></pre>
</div>
<div id="execution-error-POSTapi-v1-check-church" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-check-church"></code></pre>
</div>
<form id="form-POSTapi-v1-check-church" data-method="POST" data-path="api/v1/check/church" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-check-church', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-v1-check-church" onclick="tryItOut('POSTapi-v1-check-church');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-v1-check-church" onclick="cancelTryOut('POSTapi-v1-check-church');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-v1-check-church" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/v1/check/church</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>api_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="api_token" data-endpoint="POSTapi-v1-check-church" data-component="query" required  hidden>
<br>
Authentication key.
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>date</code></b>&nbsp;&nbsp;<small>date</small>  &nbsp;
<input type="text" name="date" data-endpoint="POSTapi-v1-check-church" data-component="body" required  hidden>
<br>
Datetime last time the app sync with the cloud data (yyyy-mm-dd hh:mm:ss).
</p>

</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
All Church that updated more than given date.
</p><h1>Auth Management</h1>
<p>APIs for User Auth</p>
<h2>Login Default</h2>
<p>Login Default, on success you'll get a 200 OK response.
Return 401 when user is not found / authenticated.
Return 422 when parameter is not filled.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost:8000/api/v1/auth/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"username":"prasetyon","password":"passw0rd","token":"aBc4e6Gh"}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/auth/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "username": "prasetyon",
    "password": "passw0rd",
    "token": "aBc4e6Gh"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<div id="execution-results-POSTapi-v1-auth-login" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-v1-auth-login"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-auth-login"></code></pre>
</div>
<div id="execution-error-POSTapi-v1-auth-login" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-auth-login"></code></pre>
</div>
<form id="form-POSTapi-v1-auth-login" data-method="POST" data-path="api/v1/auth/login" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-auth-login', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-v1-auth-login" onclick="tryItOut('POSTapi-v1-auth-login');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-v1-auth-login" onclick="cancelTryOut('POSTapi-v1-auth-login');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-v1-auth-login" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/v1/auth/login</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>username</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="username" data-endpoint="POSTapi-v1-auth-login" data-component="body" required  hidden>
<br>
User's username
</p>
<p>
<b><code>password</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="password" name="password" data-endpoint="POSTapi-v1-auth-login" data-component="body" required  hidden>
<br>
User's password.
</p>
<p>
<b><code>token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="token" data-endpoint="POSTapi-v1-auth-login" data-component="body" required  hidden>
<br>
User's device token.
</p>

</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
User list.
</p>
<h2>Login Google</h2>
<p>Login Google, on success you'll get a 200 OK response.
Return 401 when user is not found / authenticated.
Return 422 when parameter is not filled.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost:8000/api/v1/auth/google" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"email":"prasetyon@ezlife.id","token":"aBc4e6Gh"}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/auth/google"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "email": "prasetyon@ezlife.id",
    "token": "aBc4e6Gh"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<div id="execution-results-POSTapi-v1-auth-google" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-v1-auth-google"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-auth-google"></code></pre>
</div>
<div id="execution-error-POSTapi-v1-auth-google" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-auth-google"></code></pre>
</div>
<form id="form-POSTapi-v1-auth-google" data-method="POST" data-path="api/v1/auth/google" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-auth-google', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-v1-auth-google" onclick="tryItOut('POSTapi-v1-auth-google');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-v1-auth-google" onclick="cancelTryOut('POSTapi-v1-auth-google');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-v1-auth-google" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/v1/auth/google</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="email" data-endpoint="POSTapi-v1-auth-google" data-component="body" required  hidden>
<br>
User's email
</p>
<p>
<b><code>token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="token" data-endpoint="POSTapi-v1-auth-google" data-component="body" required  hidden>
<br>
User's device token.
</p>

</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
User list.
</p>
<h2>Logout</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Logout, on success you'll get a 200 OK response.
Return 401 when user is not found / authenticated.
Return 422 when parameter is not filled.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost:8000/api/v1/auth/logout?api_token=%7BYOUR_AUTH_KEY%7D" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"id":"999","token":"aBc4e6Gh"}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/auth/logout"
);

let params = {
    "api_token": "{YOUR_AUTH_KEY}",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "id": "999",
    "token": "aBc4e6Gh"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<div id="execution-results-POSTapi-v1-auth-logout" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-v1-auth-logout"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-auth-logout"></code></pre>
</div>
<div id="execution-error-POSTapi-v1-auth-logout" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-auth-logout"></code></pre>
</div>
<form id="form-POSTapi-v1-auth-logout" data-method="POST" data-path="api/v1/auth/logout" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-auth-logout', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-v1-auth-logout" onclick="tryItOut('POSTapi-v1-auth-logout');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-v1-auth-logout" onclick="cancelTryOut('POSTapi-v1-auth-logout');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-v1-auth-logout" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/v1/auth/logout</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>api_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="api_token" data-endpoint="POSTapi-v1-auth-logout" data-component="query" required  hidden>
<br>
Authentication key.
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="id" data-endpoint="POSTapi-v1-auth-logout" data-component="body" required  hidden>
<br>
User's id
</p>
<p>
<b><code>token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="token" data-endpoint="POSTapi-v1-auth-logout" data-component="body" required  hidden>
<br>
User's device token.
</p>

</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
User list.
</p><h1>Bible Management</h1>
<p>APIs for managing bible</p>
<h2>Get Bible Version Available</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Get Available Version List, on success you'll get a 200 OK response.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost:8000/api/v1/bible/version?api_token=%7BYOUR_AUTH_KEY%7D" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/bible/version"
);

let params = {
    "api_token": "{YOUR_AUTH_KEY}",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (401):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "Unauthenticated."
}</code></pre>
<div id="execution-results-GETapi-v1-bible-version" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-v1-bible-version"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-bible-version"></code></pre>
</div>
<div id="execution-error-GETapi-v1-bible-version" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-bible-version"></code></pre>
</div>
<form id="form-GETapi-v1-bible-version" data-method="GET" data-path="api/v1/bible/version" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-bible-version', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-v1-bible-version" onclick="tryItOut('GETapi-v1-bible-version');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-v1-bible-version" onclick="cancelTryOut('GETapi-v1-bible-version');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-v1-bible-version" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/v1/bible/version</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>api_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="api_token" data-endpoint="GETapi-v1-bible-version" data-component="query" required  hidden>
<br>
Authentication key.
</p>
</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Version List data.
</p>
<h2>Get Bible</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Get Bible Data based on Version, on success you'll get a 200 OK response.
Return 422 when Version parameter is not filled.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost:8000/api/v1/bible/data?api_token=%7BYOUR_AUTH_KEY%7D" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"version":"TB","book":"2"}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/bible/data"
);

let params = {
    "api_token": "{YOUR_AUTH_KEY}",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "version": "TB",
    "book": "2"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<div id="execution-results-POSTapi-v1-bible-data" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-v1-bible-data"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-bible-data"></code></pre>
</div>
<div id="execution-error-POSTapi-v1-bible-data" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-bible-data"></code></pre>
</div>
<form id="form-POSTapi-v1-bible-data" data-method="POST" data-path="api/v1/bible/data" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-bible-data', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-v1-bible-data" onclick="tryItOut('POSTapi-v1-bible-data');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-v1-bible-data" onclick="cancelTryOut('POSTapi-v1-bible-data');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-v1-bible-data" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/v1/bible/data</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>api_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="api_token" data-endpoint="POSTapi-v1-bible-data" data-component="query" required  hidden>
<br>
Authentication key.
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>version</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="version" data-endpoint="POSTapi-v1-bible-data" data-component="body" required  hidden>
<br>
The Version Name.
</p>
<p>
<b><code>book</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="book" data-endpoint="POSTapi-v1-bible-data" data-component="body" required  hidden>
<br>
The Version Name.
</p>

</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Bible Data per passage.
</p><h1>Church Management</h1>
<p>APIs for managing church</p>
<h2>Get Church Sinode</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Get Church Sinode Data, on success you'll get a 200 OK response.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost:8000/api/v1/church/sinode?api_token=%7BYOUR_AUTH_KEY%7D&amp;search=GP&amp;limit=3&amp;page=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/church/sinode"
);

let params = {
    "api_token": "{YOUR_AUTH_KEY}",
    "search": "GP",
    "limit": "3",
    "page": "1",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (401):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "Unauthenticated."
}</code></pre>
<div id="execution-results-GETapi-v1-church-sinode" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-v1-church-sinode"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-church-sinode"></code></pre>
</div>
<div id="execution-error-GETapi-v1-church-sinode" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-church-sinode"></code></pre>
</div>
<form id="form-GETapi-v1-church-sinode" data-method="GET" data-path="api/v1/church/sinode" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-church-sinode', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-v1-church-sinode" onclick="tryItOut('GETapi-v1-church-sinode');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-v1-church-sinode" onclick="cancelTryOut('GETapi-v1-church-sinode');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-v1-church-sinode" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/v1/church/sinode</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>api_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="api_token" data-endpoint="GETapi-v1-church-sinode" data-component="query" required  hidden>
<br>
Authentication key.
</p>
<p>
<b><code>search</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="search" data-endpoint="GETapi-v1-church-sinode" data-component="query"  hidden>
<br>
Church name to be searched.
</p>
<p>
<b><code>limit</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="limit" data-endpoint="GETapi-v1-church-sinode" data-component="query"  hidden>
<br>
Number of data shown.
</p>
<p>
<b><code>page</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="page" data-endpoint="GETapi-v1-church-sinode" data-component="query"  hidden>
<br>
Selected page to be shown.
</p>
</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Church Sinode Data sorted by distance.
</p>
<h2>Get Church</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Get Church Data sorted by location given, on success you'll get a 200 OK response.
Return 422 when parameter is not filled.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost:8000/api/v1/church/data?api_token=%7BYOUR_AUTH_KEY%7D" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"search":"GKI","lat":"-6.1817619","lng":"106.956194","limit":5,"page":2}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/church/data"
);

let params = {
    "api_token": "{YOUR_AUTH_KEY}",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "search": "GKI",
    "lat": "-6.1817619",
    "lng": "106.956194",
    "limit": 5,
    "page": 2
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<div id="execution-results-POSTapi-v1-church-data" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-v1-church-data"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-church-data"></code></pre>
</div>
<div id="execution-error-POSTapi-v1-church-data" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-church-data"></code></pre>
</div>
<form id="form-POSTapi-v1-church-data" data-method="POST" data-path="api/v1/church/data" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-church-data', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-v1-church-data" onclick="tryItOut('POSTapi-v1-church-data');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-v1-church-data" onclick="cancelTryOut('POSTapi-v1-church-data');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-v1-church-data" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/v1/church/data</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>api_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="api_token" data-endpoint="POSTapi-v1-church-data" data-component="query" required  hidden>
<br>
Authentication key.
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>search</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="search" data-endpoint="POSTapi-v1-church-data" data-component="body" required  hidden>
<br>
Church name to be searched.
</p>
<p>
<b><code>lat</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="lat" data-endpoint="POSTapi-v1-church-data" data-component="body" required  hidden>
<br>
Latitude of user location.
</p>
<p>
<b><code>lng</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="lng" data-endpoint="POSTapi-v1-church-data" data-component="body" required  hidden>
<br>
Longitude of user location.
</p>
<p>
<b><code>limit</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="limit" data-endpoint="POSTapi-v1-church-data" data-component="body"  hidden>
<br>
Number of data shown.
</p>
<p>
<b><code>page</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="page" data-endpoint="POSTapi-v1-church-data" data-component="body"  hidden>
<br>
Selected page to be shown.
</p>

</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Church Data sorted by distance.
</p>
<h2>Send Church Request</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Get Church Data sorted by location given, on success you'll get a 200 OK response.
Return 422 when parameter is not filled.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost:8000/api/v1/church/request?api_token=%7BYOUR_AUTH_KEY%7D" \
    -H "Content-Type: multipart/form-data" \
    -H "Accept: application/json" \
    -F "user=1" \
    -F "sinode=1" \
    -F "church=1" \
    -F "name=GPIB Persatuan" \
    -F "address=Jl. IIni iit ini ituu, Kota Begn, Beigt" \
    -F "lat=-6.1817619" \
    -F "lng=106.956194" \
    -F "phone=5" \
    -F "service=2" \
    -F "file=@/private/var/folders/9g/khq2q02d4k73jyxt1wn7b3f40000gn/T/phpzti6sE" </code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/church/request"
);

let params = {
    "api_token": "{YOUR_AUTH_KEY}",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "multipart/form-data",
    "Accept": "application/json",
};

const body = new FormData();
body.append('user', '1');
body.append('sinode', '1');
body.append('church', '1');
body.append('name', 'GPIB Persatuan');
body.append('address', 'Jl. IIni iit ini ituu, Kota Begn, Beigt');
body.append('lat', '-6.1817619');
body.append('lng', '106.956194');
body.append('phone', '5');
body.append('service', '2');
body.append('file', document.querySelector('input[name="file"]').files[0]);

fetch(url, {
    method: "POST",
    headers,
    body,
}).then(response =&gt; response.json());</code></pre>
<div id="execution-results-POSTapi-v1-church-request" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-v1-church-request"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-church-request"></code></pre>
</div>
<div id="execution-error-POSTapi-v1-church-request" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-church-request"></code></pre>
</div>
<form id="form-POSTapi-v1-church-request" data-method="POST" data-path="api/v1/church/request" data-authed="1" data-hasfiles="1" data-headers='{"Content-Type":"multipart\/form-data","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-church-request', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-v1-church-request" onclick="tryItOut('POSTapi-v1-church-request');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-v1-church-request" onclick="cancelTryOut('POSTapi-v1-church-request');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-v1-church-request" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/v1/church/request</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>api_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="api_token" data-endpoint="POSTapi-v1-church-request" data-component="query" required  hidden>
<br>
Authentication key.
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>user</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="user" data-endpoint="POSTapi-v1-church-request" data-component="body" required  hidden>
<br>
User ID.
</p>
<p>
<b><code>sinode</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="sinode" data-endpoint="POSTapi-v1-church-request" data-component="body"  hidden>
<br>
Sinode ID.
</p>
<p>
<b><code>church</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="church" data-endpoint="POSTapi-v1-church-request" data-component="body"  hidden>
<br>
Church ID when User update a church, otherwise leave it blank.
</p>
<p>
<b><code>name</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="name" data-endpoint="POSTapi-v1-church-request" data-component="body"  hidden>
<br>
Church's name.
</p>
<p>
<b><code>address</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="address" data-endpoint="POSTapi-v1-church-request" data-component="body"  hidden>
<br>
Church's address.
</p>
<p>
<b><code>lat</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="lat" data-endpoint="POSTapi-v1-church-request" data-component="body"  hidden>
<br>
Latitude of church.
</p>
<p>
<b><code>lng</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="lng" data-endpoint="POSTapi-v1-church-request" data-component="body"  hidden>
<br>
Longitude of church.
</p>
<p>
<b><code>phone</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="phone" data-endpoint="POSTapi-v1-church-request" data-component="body"  hidden>
<br>
Church phone.
</p>
<p>
<b><code>service</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="service" data-endpoint="POSTapi-v1-church-request" data-component="body"  hidden>
<br>
Church service time.
</p>
<p>
<b><code>file</code></b>&nbsp;&nbsp;<small>file</small>     <i>optional</i> &nbsp;
<input type="file" name="file" data-endpoint="POSTapi-v1-church-request" data-component="body"  hidden>
<br>
Church image.
</p>

</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Church Data sorted by distance.
</p><h1>User Management</h1>
<p>APIs for managing User Data</p>
<h2>Get User List Info</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Get User List Info, on success you'll get a 200 OK response.
Return 422 when parameter is not filled.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost:8000/api/v1/user/list?api_token=%7BYOUR_AUTH_KEY%7D" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"search":"Pr","limit":5,"page":2}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/user/list"
);

let params = {
    "api_token": "{YOUR_AUTH_KEY}",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "search": "Pr",
    "limit": 5,
    "page": 2
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<div id="execution-results-POSTapi-v1-user-list" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-v1-user-list"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-user-list"></code></pre>
</div>
<div id="execution-error-POSTapi-v1-user-list" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-user-list"></code></pre>
</div>
<form id="form-POSTapi-v1-user-list" data-method="POST" data-path="api/v1/user/list" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-user-list', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-v1-user-list" onclick="tryItOut('POSTapi-v1-user-list');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-v1-user-list" onclick="cancelTryOut('POSTapi-v1-user-list');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-v1-user-list" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/v1/user/list</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>api_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="api_token" data-endpoint="POSTapi-v1-user-list" data-component="query" required  hidden>
<br>
Authentication key.
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>search</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="search" data-endpoint="POSTapi-v1-user-list" data-component="body" required  hidden>
<br>
User name to be searched.
</p>
<p>
<b><code>limit</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="limit" data-endpoint="POSTapi-v1-user-list" data-component="body"  hidden>
<br>
Number of data shown.
</p>
<p>
<b><code>page</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="page" data-endpoint="POSTapi-v1-user-list" data-component="body"  hidden>
<br>
Selected page to be shown.
</p>

</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
User list.
</p>
    </div>
    <div class="dark-box">
                    <div class="lang-selector">
                                    <a href="#" data-language-name="bash">bash</a>
                                    <a href="#" data-language-name="javascript">javascript</a>
                            </div>
            </div>
</div>
<script>
    $(function () {
        var languages = ["bash","javascript"];
        setupLanguages(languages);
    });
</script>
</body>
</html>