# Church Management

APIs for managing church

## Get Church


Get Church Data sorted by location given, on success you'll get a 200 OK response.
Return 422 when parameter is not filled.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/v1/church/data" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"search":"GKI","lat":"-6.1817619","lng":"106.956194","limit":5,"page":2}'

```

```javascript
const url = new URL(
    "http://localhost:8000/api/v1/church/data"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "search": "GKI",
    "lat": "-6.1817619",
    "lng": "106.956194",
    "limit": 5,
    "page": 2
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


<div id="execution-results-POSTapi-v1-church-data" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-v1-church-data"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-church-data"></code></pre>
</div>
<div id="execution-error-POSTapi-v1-church-data" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-church-data"></code></pre>
</div>
<form id="form-POSTapi-v1-church-data" data-method="POST" data-path="api/v1/church/data" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-church-data', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-v1-church-data" onclick="tryItOut('POSTapi-v1-church-data');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-v1-church-data" onclick="cancelTryOut('POSTapi-v1-church-data');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-v1-church-data" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/v1/church/data</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>search</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="search" data-endpoint="POSTapi-v1-church-data" data-component="body" required  hidden>
<br>
Church name to be searched.
</p>
<p>
<b><code>lat</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="lat" data-endpoint="POSTapi-v1-church-data" data-component="body" required  hidden>
<br>
Latitude of user location.
</p>
<p>
<b><code>lng</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="lng" data-endpoint="POSTapi-v1-church-data" data-component="body" required  hidden>
<br>
Longitude of user location.
</p>
<p>
<b><code>limit</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="limit" data-endpoint="POSTapi-v1-church-data" data-component="body"  hidden>
<br>
Number of data shown.
</p>
<p>
<b><code>page</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="page" data-endpoint="POSTapi-v1-church-data" data-component="body"  hidden>
<br>
Selected page to be shown.
</p>

</form>

### Response
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Church Data sorted by distance.
</p>

## Get Church Sinode


Get Church Sinode Data, on success you'll get a 200 OK response.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/v1/church/sinode?search=GP&limit=3&page=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/v1/church/sinode"
);

let params = {
    "search": "GP",
    "limit": "3",
    "page": "1",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
{
    "success": true,
    "result": "Successfully Get Church Sinode Data",
    "data": [
        {
            "name": "GPIB",
            "desc": "Gereja Protestan di Indonesia bagian Barat"
        }
    ]
}
```
<div id="execution-results-GETapi-v1-church-sinode" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-v1-church-sinode"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-church-sinode"></code></pre>
</div>
<div id="execution-error-GETapi-v1-church-sinode" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-church-sinode"></code></pre>
</div>
<form id="form-GETapi-v1-church-sinode" data-method="GET" data-path="api/v1/church/sinode" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-church-sinode', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-v1-church-sinode" onclick="tryItOut('GETapi-v1-church-sinode');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-v1-church-sinode" onclick="cancelTryOut('GETapi-v1-church-sinode');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-v1-church-sinode" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/v1/church/sinode</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>search</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="search" data-endpoint="GETapi-v1-church-sinode" data-component="query"  hidden>
<br>
Church name to be searched.
</p>
<p>
<b><code>limit</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="limit" data-endpoint="GETapi-v1-church-sinode" data-component="query"  hidden>
<br>
Number of data shown.
</p>
<p>
<b><code>page</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="page" data-endpoint="GETapi-v1-church-sinode" data-component="query"  hidden>
<br>
Selected page to be shown.
</p>
</form>

### Response
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;<small>boolean</small>  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;<small>array</small>  &nbsp;
<br>
Church Sinode Data sorted by distance.
</p>

## Send Church Request


Get Church Data sorted by location given, on success you'll get a 200 OK response.
Return 422 when parameter is not filled.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/v1/church/request" \
    -H "Content-Type: multipart/form-data" \
    -H "Accept: application/json" \
    -F "user=1" \
    -F "sinode=1" \
    -F "church=1" \
    -F "name=GPIB Persatuan" \
    -F "address=Jl. IIni iit ini ituu, Kota Begn, Beigt" \
    -F "lat=-6.1817619" \
    -F "lng=106.956194" \
    -F "phone=5" \
    -F "service=2" \
    -F "file=@/private/var/folders/9g/khq2q02d4k73jyxt1wn7b3f40000gn/T/phpHlXZFK" 
```

```javascript
const url = new URL(
    "http://localhost:8000/api/v1/church/request"
);

let headers = {
    "Content-Type": "multipart/form-data",
    "Accept": "application/json",
};

const body = new FormData();
body.append('user', '1');
body.append('sinode', '1');
body.append('church', '1');
body.append('name', 'GPIB Persatuan');
body.append('address', 'Jl. IIni iit ini ituu, Kota Begn, Beigt');
body.append('lat', '-6.1817619');
body.append('lng', '106.956194');
body.append('phone', '5');
body.append('service', '2');
body.append('file', document.querySelector('input[name="file"]').files[0]);

fetch(url, {
    method: "POST",
    headers,
    body,
}).then(response => response.json());
```


<div id="execution-results-POSTapi-v1-church-request" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-v1-church-request"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-church-request"></code></pre>
</div>
<div id="execution-error-POSTapi-v1-church-request" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-church-request"></code></pre>
</div>
<form id="form-POSTapi-v1-church-request" data-method="POST" data-path="api/v1/church/request" data-authed="0" data-hasfiles="1" data-headers='{"Content-Type":"multipart\/form-data","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-church-request', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-v1-church-request" onclick="tryItOut('POSTapi-v1-church-request');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-v1-church-request" onclick="cancelTryOut('POSTapi-v1-church-request');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-v1-church-request" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/v1/church/request</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>user</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="user" data-endpoint="POSTapi-v1-church-request" data-component="body" required  hidden>
<br>
User ID.
</p>
<p>
<b><code>sinode</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="sinode" data-endpoint="POSTapi-v1-church-request" data-component="body"  hidden>
<br>
Sinode ID.
</p>
<p>
<b><code>church</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="church" data-endpoint="POSTapi-v1-church-request" data-component="body"  hidden>
<br>
Church ID when User update a church, otherwise leave it blank.
</p>
<p>
<b><code>name</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="name" data-endpoint="POSTapi-v1-church-request" data-component="body"  hidden>
<br>
Church's name.
</p>
<p>
<b><code>address</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="address" data-endpoint="POSTapi-v1-church-request" data-component="body"  hidden>
<br>
Church's address.
</p>
<p>
<b><code>lat</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="lat" data-endpoint="POSTapi-v1-church-request" data-component="body"  hidden>
<br>
Latitude of church.
</p>
<p>
<b><code>lng</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="lng" data-endpoint="POSTapi-v1-church-request" data-component="body"  hidden>
<br>
Longitude of church.
</p>
<p>
<b><code>phone</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="phone" data-endpoint="POSTapi-v1-church-request" data-component="body"  hidden>
<br>
Church phone.
</p>
<p>
<b><code>service</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="service" data-endpoint="POSTapi-v1-church-request" data-component="body"  hidden>
<br>
Church service time.
</p>
<p>
<b><code>file</code></b>&nbsp;&nbsp;<small>file</small>     <i>optional</i> &nbsp;
<input type="file" name="file" data-endpoint="POSTapi-v1-church-request" data-component="body"  hidden>
<br>
Church image.
</p>

</form>

### Response
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Church Data sorted by distance.
</p>


