# App Version Management

APIs for checking application and data version

## Get Latest App Version Info


Get Latest App Version Info, on success you'll get a 200 OK response.
Return 422 when parameter is not filled.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/v1/check/app" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"platform":1}'

```

```javascript
const url = new URL(
    "http://localhost:8000/api/v1/check/app"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "platform": 1
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


<div id="execution-results-POSTapi-v1-check-app" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-v1-check-app"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-check-app"></code></pre>
</div>
<div id="execution-error-POSTapi-v1-check-app" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-check-app"></code></pre>
</div>
<form id="form-POSTapi-v1-check-app" data-method="POST" data-path="api/v1/check/app" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-check-app', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-v1-check-app" onclick="tryItOut('POSTapi-v1-check-app');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-v1-check-app" onclick="cancelTryOut('POSTapi-v1-check-app');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-v1-check-app" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/v1/check/app</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>platform</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="platform" data-endpoint="POSTapi-v1-check-app" data-component="body" required  hidden>
<br>
Platform to be checked (0: Website, 1: Mobile).
</p>

</form>

### Response
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Latest Version data.
</p>

## Get Latest Church Timestamp


Get Latest Church Timestamp, on success you'll get a 200 OK response.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/v1/check/church" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"date":"2021-10-13 11:00:00"}'

```

```javascript
const url = new URL(
    "http://localhost:8000/api/v1/check/church"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "date": "2021-10-13 11:00:00"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


<div id="execution-results-POSTapi-v1-check-church" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-v1-check-church"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-check-church"></code></pre>
</div>
<div id="execution-error-POSTapi-v1-check-church" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-check-church"></code></pre>
</div>
<form id="form-POSTapi-v1-check-church" data-method="POST" data-path="api/v1/check/church" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-check-church', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-v1-check-church" onclick="tryItOut('POSTapi-v1-check-church');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-v1-check-church" onclick="cancelTryOut('POSTapi-v1-check-church');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-v1-check-church" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/v1/check/church</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>date</code></b>&nbsp;&nbsp;<small>date</small>  &nbsp;
<input type="text" name="date" data-endpoint="POSTapi-v1-check-church" data-component="body" required  hidden>
<br>
Datetime last time the app sync with the cloud data (yyyy-mm-dd hh:mm:ss).
</p>

</form>

### Response
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
All Church that updated more than given date.
</p>


