# Bible Management

APIs for managing bible

## Get Bible Version Available


Get Available Version List, on success you'll get a 200 OK response.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/v1/bible/version" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/v1/bible/version"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
{
    "success": true,
    "result": "Successfully Get Bible Version List",
    "data": [
        {
            "id": 1,
            "name": "TB",
            "text": "Terjemahan Baru",
            "language": "Bahasa Indonesia",
            "source": "sabda.org",
            "deleted_at": null,
            "created_at": "2021-10-10T12:29:23.000000Z",
            "updated_at": "2021-10-10T12:29:23.000000Z"
        },
        {
            "id": 2,
            "name": "TL",
            "text": "Terjemahan Lama",
            "language": "Bahasa Indonesia",
            "source": "sabda.org",
            "deleted_at": null,
            "created_at": "2021-10-10T12:29:23.000000Z",
            "updated_at": "2021-10-10T12:29:23.000000Z"
        }
    ]
}
```
<div id="execution-results-GETapi-v1-bible-version" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-v1-bible-version"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-bible-version"></code></pre>
</div>
<div id="execution-error-GETapi-v1-bible-version" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-bible-version"></code></pre>
</div>
<form id="form-GETapi-v1-bible-version" data-method="GET" data-path="api/v1/bible/version" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-bible-version', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-v1-bible-version" onclick="tryItOut('GETapi-v1-bible-version');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-v1-bible-version" onclick="cancelTryOut('GETapi-v1-bible-version');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-v1-bible-version" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/v1/bible/version</code></b>
</p>
</form>

### Response
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;<small>boolean</small>  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;<small>array</small>  &nbsp;
<br>
Version List data.
</p>

## Get Bible


Get Bible Data based on Version, on success you'll get a 200 OK response.
Return 422 when Version parameter is not filled.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/v1/bible/data" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"version":"TB","book":63}'

```

```javascript
const url = new URL(
    "http://localhost:8000/api/v1/bible/data"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "version": "TB",
    "book": 63
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


<div id="execution-results-POSTapi-v1-bible-data" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-v1-bible-data"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-bible-data"></code></pre>
</div>
<div id="execution-error-POSTapi-v1-bible-data" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-bible-data"></code></pre>
</div>
<form id="form-POSTapi-v1-bible-data" data-method="POST" data-path="api/v1/bible/data" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-bible-data', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-v1-bible-data" onclick="tryItOut('POSTapi-v1-bible-data');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-v1-bible-data" onclick="cancelTryOut('POSTapi-v1-bible-data');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-v1-bible-data" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/v1/bible/data</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>version</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="version" data-endpoint="POSTapi-v1-bible-data" data-component="body" required  hidden>
<br>
The Version Name.
</p>
<p>
<b><code>book</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="book" data-endpoint="POSTapi-v1-bible-data" data-component="body" required  hidden>
<br>
The Book Number.
</p>

</form>

### Response
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Bible Data per passage.
</p>


